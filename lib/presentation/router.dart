import 'package:base_code_project/model/repo/barrel_repo.dart';
import 'package:base_code_project/presentation/screen/calendar_reminder/sc_calendar_reminder .dart';
import 'package:base_code_project/presentation/screen/chat_with_doctor/chat_with_doctor.dart';
import 'package:base_code_project/presentation/screen/detail_comment/sc_social_list_detail.dart';
import 'package:base_code_project/presentation/screen/forgot_pass/forgot_pass_sc.dart';
import 'package:base_code_project/presentation/screen/forgot_pass_reset/forgot_password_reset_page_sc.dart';
import 'package:base_code_project/presentation/screen/forgot_vefical/forgot_password_verify_sc.dart';
import 'package:base_code_project/presentation/screen/history_question/history_question.dart';
import 'package:base_code_project/presentation/screen/list_doctor/sc_listdoctor.dart';
import 'package:base_code_project/presentation/screen/login/login_sc.dart';
import 'package:base_code_project/presentation/screen/login_phone/sc_LoginPhonePage.dart';
import 'package:base_code_project/presentation/screen/login_phone/sc_enterPhoneNumber.dart';
import 'package:base_code_project/presentation/screen/menu/profile/address/add_address/sc_add_address.dart';
import 'package:base_code_project/presentation/screen/menu/profile/address/edit_address/sc_edit_address.dart';
import 'package:base_code_project/presentation/screen/menu/profile/address/sc_address.dart';
import 'package:base_code_project/presentation/screen/menu/profile/family_profile/add_person/sc_add_person.dart';
import 'package:base_code_project/presentation/screen/menu/profile/family_profile/sc_family_profile.dart';
import 'package:base_code_project/presentation/screen/menu/profile/history_advisory/history_advisory.dart';
import 'package:base_code_project/presentation/screen/menu/profile/test_results/sc_test_results.dart';
import 'package:base_code_project/presentation/screen/navigation/sc_navigation.dart';
import 'package:base_code_project/presentation/screen/new/sc_new.dart';
import 'package:base_code_project/presentation/screen/notification_account/sc_notification_account.dart';
import 'package:base_code_project/presentation/screen/post_social/sc_post_news.dart';
import 'package:base_code_project/presentation/screen/register/register_sc.dart';
import 'package:base_code_project/presentation/screen/register_vefical/register_verify_sc.dart';
import 'package:base_code_project/presentation/screen/seach_questions/sc_seach_questions.dart';
import 'package:base_code_project/presentation/screen/service_price/detail_service_screen.dart';
import 'package:base_code_project/presentation/screen/service_price/sc_servicePrice.dart';
import 'package:base_code_project/presentation/screen/splash/sc_splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/loginPhone';
  static const String NAVIGATION = '/navigation';
  static const String HISTORY_QUESTION = '/history_question';
  static const String HISTORY_ADVISORY = '/history_advisory';
  static const String FAMILY_PROFILE = '/family_profile';
  static const String ADD_PERSON = '/add_person';
  static const String ENTERPHONENUMBER = '/EnterPhonePage';
  static const String REGISTER_PAGE = '/register_page';
  static const String FORGOT_PASS = '/forgot_pass';
  static const String FORGOT_PASS_VERIFICAL = '/forgot_pass_verifi';
  static const String FORGOT_PASS_RESET = '/forgot_pass_reset';
  static const String REGISTER_VERIFY = '/register_verify';
  static const String CHAT_WITH_DOCTOR = '/chat_with_doctor';
  static const String TEST_RESULT = '/test_result';
  static const String LIST_DOCTOR = '/list_doctor';
  static const String LIST_NUTRITION = '/list_nutrition';
  static const String LIST_COMMENT_DETAIL = '/list_detail_comment';
  static const String NEWS_POST = '/news_post';
  static const String NEWS_SCREEN = '/news_screen';
  static const String DETAIL_SERVICE_SCREEN = '/detail_service_screen';
  static const String ADDRESS_SCREEN = '/address_screen';
  static const String ADD_ADDRESS_SCREEN = '/new_address_screen';
  static const String EDIT_ADDRESS_SCREEN = '/edit_address_screen';
  static const String SEACH_QUESTION_SCREEN = '/question_screen';
  static const String NOTIFICATION_ACCOUNT_SCREEN = '/account_screen';
  static const String CALENDAR_REMINDER_SCREEN = '/reminder_screen';
  static const String NEWS_PAPER_SCREEN = '/news_paper_screen';
  static const String SERVICE_SCREEN = '/service_screen';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case LOGIN:
        return MaterialPageRoute(builder: (_) => LoginPhonePage());

      case ENTERPHONENUMBER:
        return MaterialPageRoute(builder: (_) => EnterPhonePage());

      case HISTORY_QUESTION:
        return MaterialPageRoute(builder: (_) => HistoryQuestionScreen());

      case NAVIGATION:
        return MaterialPageRoute(builder: (_) => NavigationScreen());

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var homeRepository = RepositoryProvider.of<HomeRepository>(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    var notificationRepository =
        RepositoryProvider.of<NotificationRepository>(context);
    var addressRepository = RepositoryProvider.of<AddressRepository>(context);
    var cartRepository = RepositoryProvider.of<CartRepository>(context);
    var paymentRepository = RepositoryProvider.of<PaymentRepository>(context);
    var invoiceRepository = RepositoryProvider.of<InvoiceRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      LOGIN: (context) => LoginPhonePage(),
      NAVIGATION: (context) => NavigationScreen(),
      HISTORY_QUESTION: (context) => HistoryQuestionScreen(),
      HISTORY_ADVISORY: (context) => HistoryAdvisoryScreen(),
      FAMILY_PROFILE: (context) => FamilyProfileScreen(),
      ADD_PERSON: (context) => AddPersonScreen(),
      ENTERPHONENUMBER: (context) => LoginPage(),
      REGISTER_PAGE: (context) => RegisterPage(),
      FORGOT_PASS: (context) => ForgotPassPage(),
      FORGOT_PASS_VERIFICAL: (context) => ForgotPassVerifyPage(),
      FORGOT_PASS_RESET: (context) => ForgotPassResetPage(),
      REGISTER_VERIFY: (context) => RegisterVerifyPage(),
      CHAT_WITH_DOCTOR: (context) => ChatWithDoctorScreen(),
      TEST_RESULT: (context) => TestResultScreen(),
      LIST_DOCTOR: (context) => ListDoctorScreen(),
      LIST_COMMENT_DETAIL: (context) => NewsDetailNewsPage(),
      NEWS_POST: (context) => WidgetPostsFormScreen(),
      NEWS_SCREEN: (context) => NewScreen(),
      DETAIL_SERVICE_SCREEN: (context) => DetailServiceScreen(),
      ADDRESS_SCREEN: (context) => AddressScreen(),
      ADD_ADDRESS_SCREEN: (context) => AddAddressScreen(),
      EDIT_ADDRESS_SCREEN: (context) => EditAddressScreen(),
      SEACH_QUESTION_SCREEN: (context) => SeachQuestionScreen(),
      NOTIFICATION_ACCOUNT_SCREEN: (context) => NotificationAccountScreen(),
      CALENDAR_REMINDER_SCREEN: (context) => CalendarReminderScreen(),
      SERVICE_SCREEN: (context) => ServiceScreen(),
    };
  }
}
