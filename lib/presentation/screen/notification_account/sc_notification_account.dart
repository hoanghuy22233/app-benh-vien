import 'package:base_code_project/model/enitity_offline/categoryseach.dart';
import 'package:base_code_project/model/enitity_offline/product_list_doctor.dart';
import 'package:base_code_project/model/repo/barrel_repo.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/login/barrel_login.dart';
import 'package:base_code_project/presentation/screen/login/bloc/bloc.dart';
import 'package:base_code_project/presentation/screen/menu/social/category_social.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationAccountScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // var userRepository = RepositoryProvider.of<UserRepository>(context);

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        // scrollDirection: Axis.vertical,
        child: ListView(
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.05,
                    color: Colors.white,
                  ),
                  Positioned.fill(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            child: Icon(
                              Icons.arrow_back_ios_sharp,
                              size: 20,
                            ),
                          ),
                        ), // SizedBox(width: 90,),
                        Expanded(
                          flex: 8,
                          child: Text(
                            'Thông tin tài khoản',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Text(
                              "Lưu",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            )),

                        //tensanpham
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: 20,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(500),
                  child: Image.asset(
                    "assets/icons/nav_user.png",
                    height: 40,
                    // fit: BoxFit.fill,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  flex: 7,
                  child: Text(
                    "Ảnh đại diện",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    "Thay đổi",
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.blue,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 1,
                    height: MediaQuery.of(context).size.height * 0.02,
                    color: Colors.grey[100],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                            flex: 9,
                            child: TextField(
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                   hintText: "Họ tên",
                                   hintStyle: TextStyle(color: Colors.grey),
                                  labelText: "Họ tên ",
                                  labelStyle: TextStyle(
                                    color: Colors.grey,
                                  ),
                                  suffixIcon: Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 10,
                                  )
                                  ),
                            )),
                        Expanded(
                            flex: 1,
                            child: Icon(Icons.keyboard_arrow_down_outlined))
                      ],
                    ),
                  ),
                  Container(
                    child: Divider(
                      thickness: 1,
                      color: Colors.grey[300],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                            flex: 9,
                            child: TextField(
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Giới tính",
                                  hintStyle: TextStyle(color: Colors.grey),
                                  labelText: "Giới tinh",
                                  labelStyle: TextStyle(
                                    color: Colors.grey,
                                  ),
                                  suffixIcon: Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 10,
                                  )
                              ),
                            )),
                        Expanded(
                            flex: 1,
                            child: Icon(Icons.keyboard_arrow_down_outlined))
                      ],
                    ),
                  ),
                  Container(
                    child: Divider(
                      thickness: 1,
                      color: Colors.grey[300],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                            flex: 9,
                            child: TextField(
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "0987654321",
                                  hintStyle: TextStyle(color: Colors.black),
                                  labelText: "Số điện thoại ",
                                  labelStyle: TextStyle(
                                    color: Colors.grey,
                                  ),
                                  suffixIcon: Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 10,
                                  )
                              ),
                            )),
                        Expanded(
                            flex: 1,
                            child: Icon(Icons.keyboard_arrow_down_outlined))
                      ],
                    ),
                  ),
                  Container(
                    child: Divider(
                      thickness: 1,
                      color: Colors.grey[300],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                            flex: 9,
                            child: TextField(
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Email",
                                  hintStyle: TextStyle(color: Colors.grey),
                                  labelText: "Email",
                                  labelStyle: TextStyle(
                                    color: Colors.grey,
                                  ),
                                  suffixIcon: Icon(
                                    Icons.star,
                                    color: Colors.red,
                                    size: 10,
                                  )
                              ),
                            )),
                        Expanded(
                            flex: 1,
                            child: Icon(Icons.keyboard_arrow_down_outlined))
                      ],
                    ),
                  ),
                  Container(
                    child: Divider(
                      thickness: 1,
                      color: Colors.grey[300],
                    ),
                  ),



                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 1,
                    height: MediaQuery.of(context).size.height * 0.02,
                    color: Colors.grey[100],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Center(
              child: Text("Đăng xuất"),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 1,
                    height: MediaQuery.of(context).size.height * 0.5,
                    color: Colors.grey[100],
                  ),
                ],
              ),
            ),


          ],
        ),
      ),
    );
  }
}
