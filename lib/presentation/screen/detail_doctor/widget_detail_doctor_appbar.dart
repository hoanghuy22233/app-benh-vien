import 'package:base_code_project/presentation/common_widgets/widget_appbar.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:flutter/material.dart';

class WidgetDetailDoctorAppbar extends StatelessWidget {
  final String titleText;

  const WidgetDetailDoctorAppbar({Key key, this.titleText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: WidgetAppbar(
        left: [WidgetAppbarMenuBack()],
        title: "Thông tin Bác sĩ",
      ),
    );
  }
}
