import 'package:base_code_project/app/auth_bloc/authentication_bloc.dart';
import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/enitity_offline/categoryseach.dart';
import 'package:base_code_project/model/enitity_offline/product_list_doctor.dart';
import 'package:base_code_project/model/repo/barrel_repo.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/login/barrel_login.dart';
import 'package:base_code_project/presentation/screen/login/bloc/bloc.dart';
import 'package:base_code_project/presentation/screen/menu/social/category_social.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'list_doctor.dart';

class ListDoctorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // var userRepository = RepositoryProvider.of<UserRepository>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        // scrollDirection: Axis.vertical,
        child: ListView(
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.05,
                    color: Colors.white,
                  ),
                  Positioned.fill(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            child: Icon(
                              Icons.arrow_back_ios_sharp,
                              size: 20,
                            ),
                          ),
                        ), // SizedBox(width: 90,),
                        Expanded(
                          flex: 8,
                          child: Text(
                            'Danh sách Bác sĩ',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Image.asset(
                            "assets/images/img_facebook.png",
                            height: 20,
                          ),
                        ),

                        //tensanpham
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(
                          "Danh mục",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                        Text(
                          "Xem tất cả danh mục",
                          style: TextStyle(color: Colors.blue),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 40,
                    margin: EdgeInsets.symmetric(horizontal: 1),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: categorySeach.length + 1,
                      itemBuilder: (context, index) {
                        if (index == 0) {
                          return GestureDetector(
                            onTap: () {},
                            child: Row(
                              children: [
                                Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(500),
                                  ),
                                  child: Container(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3.6,
                                        height:
                                            MediaQuery.of(context).size.height,
                                        color: Colors.blue[100],
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(500),
                                              child: Container(
                                                color: Colors.blue[300],
                                                child: Image.asset(
                                                  "assets/images/more2.png",
                                                  height: 25,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "Tất cả",
                                              style: TextStyle(
                                                  color: Colors.black),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        }
                        return Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          child: CategorySocial(
                            id: index - 1,
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Container(
              child: Stack(
                children: [
                  Container(
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 1,
                          height: MediaQuery.of(context).size.height * 0.03,
                          color: Colors.grey[50],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              // margin: EdgeInsets.symmetric(horizontal: 10),
              color: Colors.white,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: productListDoctor.length ,
                itemBuilder: (context, index) {
                  // if (index == 0) {
                  //   return GestureDetector(
                  //     onTap: () {},
                  //     // child: Row(
                  //     //   children: [
                  //     //     Card(
                  //     //       shape: RoundedRectangleBorder(
                  //     //         borderRadius: BorderRadius.circular(500),
                  //     //       ),
                  //     //       child: Container(
                  //     //         child: ClipRRect(
                  //     //           borderRadius: BorderRadius.circular(20),
                  //     //           child:
                  //     //         ),
                  //     //       ),
                  //     //     )
                  //     //   ],
                  //     // ),
                  //   );
                  // }
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: ListDoctor(
                      id: index ,
                    ),
                  );
                },
              ),
            ),
            Container(
              child: Stack(
                children: [
                  Container(
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 1,
                          height: 1,
                          color: Colors.grey[50],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),




          ],
        ),
      ),
    );
  }
}
