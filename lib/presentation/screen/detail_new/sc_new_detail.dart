import 'package:base_code_project/model/enitity_offline/list_new_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class NewsDetail extends StatefulWidget {
  final int id;
  NewsDetail({Key key, this.id}) : super(key: key);
  _NewsDetailState createState() => new _NewsDetailState();
}

class _NewsDetailState extends State<NewsDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              child: Stack(
                alignment: Alignment.topLeft,
                children: <Widget>[
                  Container(
                    height: 200,
                    child: Swiper(
                      itemBuilder: (BuildContext context, int index) {
                        return new Image.asset(
                          listNews[widget.id].image,
                          fit: BoxFit.cover,
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                        );
                      },
                      pagination: new SwiperPagination(
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height / 4),
                        builder: new DotSwiperPaginationBuilder(
                            color: Colors.blue[300],
                            activeColor: Colors.blue[100]),
                      ),

                      autoplay: false,
                      itemCount: 3,
                      scrollDirection: Axis.horizontal,
                      // control: new SwiperControl(),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10, top: 20),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_ios_sharp,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Column(
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Row(
                          children: [
                            Text(
                              "Chia sẻ",
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Image.asset(
                              "assets/images/img_share_color.png",
                              width: 20,
                              height: 20,
                            ),
                          ],
                        )),
                        Container(
                            child: Row(
                          children: [
                            Text(
                              "Yêu thích",
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            listNews[widget.id].like == 0
                                ? Image.asset(
                                    "assets/icons/ic_heart_gray.png",
                                    width: 20,
                                    height: 20,
                                  )
                                : Image.asset(
                                    "assets/images/img_heart_actived.png",
                                    width: 20,
                                    height: 20,
                                  ),
                          ],
                        )),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(
                          listNews[widget.id].name,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.blue),
                        )),
                        Text(
                          listNews[widget.id].date,
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                listNews[widget.id].content,
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
