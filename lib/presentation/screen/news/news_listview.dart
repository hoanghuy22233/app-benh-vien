import 'package:base_code_project/app/constants/color/color.dart';

import 'package:base_code_project/model/enitity_offline/news.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewsListView extends StatefulWidget {
  final int id;
  NewsListView({Key key, this.id}) : super(key: key);
  @override
  _NewsListView createState() => _NewsListView();
}

class _NewsListView extends State<NewsListView> {
  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 2,
        child: Stack(
          children: [
            Positioned(
              top: 10,
                right: 10,
                child: Image.asset(
              "assets/images/icon_dot.png", height: 7,
              color: Colors.red,
            )),
            Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${newsItem[widget.id].title}',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    '${newsItem[widget.id].content}',
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 12),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text('${newsItem[widget.id].time}',
                      style: TextStyle(fontSize: 12)),

                ],
              ),
            )
          ],
        ));
  }
}
