import 'package:base_code_project/presentation/common_widgets/widget_appbar.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetHeathNewsAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 20),
      child: WidgetAppbar(
        title: AppLocalizations.of(context).translate('news_heath'),
        left: [WidgetAppbarMenuBack()],
        right: [
          Icon(
            Icons.more_horiz,
            size: 30,
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }
}
