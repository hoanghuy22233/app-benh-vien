import 'package:base_code_project/model/enitity_offline/news.dart';
import 'package:base_code_project/presentation/screen/news/widget_healthNews%20_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'news_listview.dart';


class NewsScreen extends StatefulWidget {
  @override
  _NewsScreen createState() => _NewsScreen();
}

class _NewsScreen extends State<NewsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:
        Column(
          children: [
            _buildAppBar(),

            Expanded(child: ListView.builder(
              padding: EdgeInsets.zero,
              scrollDirection: Axis.vertical,
              itemCount: newsItem.length,
              itemBuilder: (context, index) {
                return Container(
                  child: NewsListView(
                    id: index,
                  ),
                );
              },
            ),)
          ],
        )
    );
  }
  _buildAppBar () => WidgetHeathNewsAppbar();
}
