import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/model/enitity_offline/category_new.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryNew extends StatelessWidget {
  final int id;
  CategoryNew({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onTap: () => Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => DetailScreen(id: id)
      //   ),
      // ),
      child: Row(
        children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(500),
            ),
            child: Container(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Container(
                  width: MediaQuery.of(context).size.width / 4,
                  height: MediaQuery.of(context).size.height / 5,
                  color: Colors.grey[50],
                  child: Center(
                    child: Text(
                      categoryNews[id].name.length <= 12
                          ? categoryNews[id].name
                          : categoryNews[id].name.substring(0, 12) + '...',
                      style: AppStyle.DEFAULT_SMALL.copyWith(
                          color: AppColor.GREY_DARK_3,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
