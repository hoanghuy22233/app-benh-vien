import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/model/enitity_offline/category_new.dart';
import 'package:base_code_project/model/enitity_offline/list_new_item.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:base_code_project/presentation/screen/new/category_new.dart';
import 'package:base_code_project/presentation/screen/new/list_new/widget_list_news_main.dart';
import 'package:base_code_project/presentation/screen/new/widget_new_appbar.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MethodNew extends StatefulWidget {
  final int id;
  MethodNew ({Key key, this.id}) : super(key: key);

  @override
  _MethodNewState createState() => _MethodNewState();
}

class _MethodNewState extends State<MethodNew >  with SingleTickerProviderStateMixin {
  String textNode = 'Tất cả danh mục chuyên môn';
  final _numPages = 4;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 5.0),
      height: 8.0,
      width: isActive ? 34.0 : 12.0,
      decoration: BoxDecoration(
        color: isActive ? Colors.blue :Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
  @override

  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        NestedScrollView(
          headerSliverBuilder:
              (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                leading: Container(),

                expandedHeight: MediaQuery.of(context).size.height / 1.7,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  background: _buildContent(),
                ),
              ),
            ];
          },
          body:  Container(
              height: MediaQuery.of(context).size.height,
              child:_buildListNew()),),
        // _buildHomeContent(),
      ],
    );
  }
  _buildListNew() => WidgetListNewsMain ();


  Widget _buildContent() {
    return Container(
      child: SingleChildScrollView(
        child:
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              height: MediaQuery.of(context).size.height/3,
              child: PageView(
                physics: ClampingScrollPhysics(),
                controller: _pageController,
                onPageChanged: (int page) {
                  setState(() {
                    _currentPage = page;
                  });
                },
                children: <Widget>[
                  Stack(
                    alignment: Alignment.bottomLeft,
                    children: <Widget>[
                      Center(
                        child:  ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image(
                            image: AssetImage(
                              'assets/images/doctor_new.jpg',
                            ),
                            height: MediaQuery.of(context).size.height/3,
                            width: MediaQuery.of(context).size.width,fit: BoxFit.cover,
                          ),
                        ),

                      ),


                      Container(
                        margin: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                        child:  Text(
                          'Dinh dưỡng',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26.0,
                            height: 1.3,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Stack(
                    alignment: Alignment.bottomLeft,
                    children: <Widget>[
                      Center(
                        child:  ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image(
                            image: AssetImage(
                              'assets/images/doctor_new.jpg',
                            ),
                            height: MediaQuery.of(context).size.height/3,
                            width: MediaQuery.of(context).size.width,fit: BoxFit.cover,
                          ),
                        ),

                      ),


                      Container(
                        margin: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                        child:  Text(
                          'Dinh dưỡng',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26.0,
                            height: 1.3,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Stack(
                    alignment: Alignment.bottomLeft,
                    children: <Widget>[
                      Center(
                        child:  ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image(
                            image: AssetImage(
                              'assets/images/doctor_new.jpg',
                            ),
                            height: MediaQuery.of(context).size.height/3,
                            width: MediaQuery.of(context).size.width,fit: BoxFit.cover,
                          ),
                        ),

                      ),


                      Container(
                        margin: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                        child:  Text(
                          'Dinh dưỡng',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26.0,
                            height: 1.3,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Stack(
                    alignment: Alignment.bottomLeft,
                    children: <Widget>[
                      Center(
                        child:  ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image(
                            image: AssetImage(
                              'assets/images/doctor_new.jpg',
                            ),
                            height: MediaQuery.of(context).size.height/3,
                            width: MediaQuery.of(context).size.width,fit: BoxFit.cover,
                          ),
                        ),

                      ),


                      Container(
                        margin: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                        child:  Text(
                          'Dinh dưỡng',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26.0,
                            height: 1.3,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: _buildPageIndicator(),
                ),
                Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 14,),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Stack(
                children: [
                  Container(
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 1,
                          height: MediaQuery.of(context).size.height * 0.02,
                          color: Colors.grey[100],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(40),
              child: Container(
                color: Colors.deepPurple,
                margin: EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      "Dịch viêm phổi Corona",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.white,size: 14,
                    )
                  ],
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
