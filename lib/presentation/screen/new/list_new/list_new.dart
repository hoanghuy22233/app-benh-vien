import 'package:base_code_project/model/enitity_offline/list_new_item.dart';
import 'package:base_code_project/presentation/screen/detail_new/sc_new_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetListNews extends StatelessWidget {
  final int id;
  WidgetListNews({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => NewsDetail(id: id)),
      ),
      child: Card(
        elevation: 2,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 6,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            listNews[id].name.length <= 10
                                ? listNews[id].name
                                : listNews[id].name.substring(0, 10) + '...',
                            textAlign: TextAlign.center,
                            maxLines: 2,
                            style: TextStyle(color: Colors.blue),
                          ),
                          SizedBox(
                              width: MediaQuery.of(context).size.width / 10),
                          Text(listNews[id].date,
                              style: TextStyle(color: Colors.grey))
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        listNews[id].content.length <= 20
                            ? listNews[id].content
                            : listNews[id].content.substring(0, 20) + '...',
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                )),
            Expanded(
              flex: 4,
              child: Stack(
                alignment: Alignment.topRight,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Image.asset(
                      listNews[id].image,
                      height: MediaQuery.of(context).size.height / 6,
                      width: MediaQuery.of(context).size.width / 2,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 10, top: 10),
                    child: listNews[id].like == 0
                        ? Image.asset(
                            'assets/icons/ic_heart_gray.png',
                            width: 25,
                            height: 25,
                            color: Colors.white,
                          )
                        : Image.asset(
                            'assets/icons/ic_heart_red.png',
                            width: 25,
                            height: 25,
                          ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
