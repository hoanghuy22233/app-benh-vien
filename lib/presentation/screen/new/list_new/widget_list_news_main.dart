import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/enitity_offline/askDoctor.dart';
import 'package:base_code_project/model/enitity_offline/category.dart';
import 'package:base_code_project/model/enitity_offline/list_doctor.dart';
import 'package:base_code_project/model/enitity_offline/list_new_item.dart';
import 'package:base_code_project/presentation/screen/menu/home/category_all.dart';
import 'package:base_code_project/presentation/screen/menu/home/list_doctor/widget_list_item_doctor.dart';
import 'package:base_code_project/presentation/screen/new/list_new/list_new.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class WidgetListNewsMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: listNews.length,
        itemBuilder: (context, index) {
          return Container(

            child: WidgetListNews(
              id: index,
            ),
          );
        },
      ),
    );
  }


}
