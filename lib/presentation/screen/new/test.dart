import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/model/enitity_offline/category_new.dart';
import 'package:base_code_project/model/enitity_offline/list_new_item.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:base_code_project/presentation/screen/new/category_new.dart';
import 'package:base_code_project/presentation/screen/new/list_new/widget_list_news_main.dart';
import 'package:base_code_project/presentation/screen/new/widget_new_appbar.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewScreen extends StatefulWidget {
  final int id;
  NewScreen({Key key, this.id}) : super(key: key);

  @override
  _NewScreenState createState() => _NewScreenState();
}

class _NewScreenState extends State<NewScreen>  with SingleTickerProviderStateMixin {
  String textNode = 'Tất cả danh mục chuyên môn';
  final _numPages = 4;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  TabController _tabController;
  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 5.0),
      height: 8.0,
      width: isActive ? 34.0 : 12.0,
      decoration: BoxDecoration(
        color: isActive ? Colors.blue :Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
  @override

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Stack(
          children: <Widget>[
            NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    leading: Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          onPressed: () {
                            AppNavigator.navigateBack();

                          }),
                    ),

                    expandedHeight: MediaQuery.of(context).size.height / 1.4,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      background: _buildContent(),
                    ),
                  ),
                ];
              },
              body:  Container(
                  height: MediaQuery.of(context).size.height,
                  child:_buildListNew()),),
            _buildHomeContent(),
          ],
        ),
      ),
    );
  }

  _buildAppbar() => WidgetNewsAppbar();
  _buildListNew() => WidgetListNewsMain ();

  Widget _buildHomeContent() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildAppbar(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child:  Card(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 5),
                height: 30,
                color: Colors.grey[100],
                child: Center(
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Image.asset(
                          "assets/images/img_search.png",
                          width: 20,
                          height: 25,
                        ),
                      ),
                      Text(" "),
                      Container(
                        width: 1,
                        height: 20,
                        color: Colors.grey[300],
                      ),
                      Text("  "),
                      Expanded(
                        child: TextFormField(
                          enableInteractiveSelection: false,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(bottom: 12),
                              border: InputBorder.none,
                              hintText: "Tìm kiếm thông tin",
                              hintStyle:
                              TextStyle(color: Colors.grey[400])),
                          // textAlignVertical: TextAlignVertical.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }
  Widget _buildContent() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            height: MediaQuery.of(context).size.height/3,
            child: PageView(
              physics: ClampingScrollPhysics(),
              controller: _pageController,
              onPageChanged: (int page) {
                setState(() {
                  _currentPage = page;
                });
              },
              children: <Widget>[
                Stack(
                  alignment: Alignment.bottomLeft,
                  children: <Widget>[
                    Center(
                      child:  ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image(
                          image: AssetImage(
                            'assets/images/doctor_new.jpg',
                          ),
                          height: MediaQuery.of(context).size.height/3,
                          width: MediaQuery.of(context).size.width,fit: BoxFit.cover,
                        ),
                      ),

                    ),


                    Container(
                      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                      child:  Text(
                        'Dinh dưỡng',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 26.0,
                          height: 1.3,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                Stack(
                  alignment: Alignment.bottomLeft,
                  children: <Widget>[
                    Center(
                      child:  ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image(
                          image: AssetImage(
                            'assets/images/doctor_new.jpg',
                          ),
                          height: MediaQuery.of(context).size.height/3,
                          width: MediaQuery.of(context).size.width,fit: BoxFit.cover,
                        ),
                      ),

                    ),


                    Container(
                      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                      child:  Text(
                        'Dinh dưỡng',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 26.0,
                          height: 1.3,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                Stack(
                  alignment: Alignment.bottomLeft,
                  children: <Widget>[
                    Center(
                      child:  ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image(
                          image: AssetImage(
                            'assets/images/doctor_new.jpg',
                          ),
                          height: MediaQuery.of(context).size.height/3,
                          width: MediaQuery.of(context).size.width,fit: BoxFit.cover,
                        ),
                      ),

                    ),


                    Container(
                      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                      child:  Text(
                        'Dinh dưỡng',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 26.0,
                          height: 1.3,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                Stack(
                  alignment: Alignment.bottomLeft,
                  children: <Widget>[
                    Center(
                      child:  ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image(
                          image: AssetImage(
                            'assets/images/doctor_new.jpg',
                          ),
                          height: MediaQuery.of(context).size.height/3,
                          width: MediaQuery.of(context).size.width,fit: BoxFit.cover,
                        ),
                      ),

                    ),


                    Container(
                      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                      child:  Text(
                        'Dinh dưỡng',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 26.0,
                          height: 1.3,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: _buildPageIndicator(),
              ),
              Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 14,),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            child: Stack(
              children: [
                Container(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 1,
                        height: MediaQuery.of(context).size.height * 0.02,
                        color: Colors.grey[100],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Container(
              color: Colors.deepPurple,
              margin: EdgeInsets.symmetric(horizontal: 10),
              width: MediaQuery.of(context).size.width,
              height: 40,
              child: Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "Dịch viêm phổi Corona",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.white,size: 14,
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
              height: MediaQuery.of(context).size.height,
              child:_buildListNew()),

        ],
      ),
    );
  }
}
