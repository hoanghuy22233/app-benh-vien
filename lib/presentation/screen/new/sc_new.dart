import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/model/enitity_offline/category_new.dart';
import 'package:base_code_project/model/enitity_offline/list_new_item.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:base_code_project/presentation/screen/new/all_new/all_new.dart';
import 'package:base_code_project/presentation/screen/new/category_new.dart';
import 'package:base_code_project/presentation/screen/new/list_new/widget_list_news_main.dart';
import 'package:base_code_project/presentation/screen/new/new_method/method_new.dart';
import 'package:base_code_project/presentation/screen/new/new_new/new_new.dart';
import 'package:base_code_project/presentation/screen/new/new_sale/sale_new.dart';
import 'package:base_code_project/presentation/screen/new/widget_new_appbar.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewScreen extends StatefulWidget {
  final int id;
  NewScreen({Key key, this.id}) : super(key: key);

  @override
  _NewScreenState createState() => _NewScreenState();
}

class _NewScreenState extends State<NewScreen>  with SingleTickerProviderStateMixin {
  String textNode = 'Tất cả danh mục chuyên môn';
  final _numPages = 4;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: Container(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildAppbar(),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child:  Card(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      height: 30,
                      color: Colors.grey[100],
                      child: Center(
                        child: Row(
                          // mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Image.asset(
                                "assets/images/img_search.png",
                                width: 20,
                                height: 25,
                              ),
                            ),
                            Text(" "),
                            Container(
                              width: 1,
                              height: 20,
                              color: Colors.grey[300],
                            ),
                            Text("  "),
                            Expanded(
                              child: TextFormField(
                                enableInteractiveSelection: false,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(bottom: 12),
                                    border: InputBorder.none,
                                    hintText: "Tìm kiếm thông tin",
                                    hintStyle:
                                    TextStyle(color: Colors.grey[400])),
                                // textAlignVertical: TextAlignVertical.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                  child:  Text("Danh mục",style: TextStyle(fontWeight: FontWeight.bold),),
                ),
                _buildTabBarMenu(),
                Expanded(
                  child: TabBarView(
                    controller: _tabController,
                    children: [NewAll(), NewNews(),NewSale(),MethodNew()],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  _buildAppbar() => WidgetNewsAppbar();

  Widget _buildTabBarMenu() {
    return new Container(
      // decoration: BoxDecoration(
      //     border: Border.all(color: AppColor.BLACK),
      //     borderRadius: BorderRadius.circular(500)),
    //  height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        tabs: [
          Tab(
            text: "Tất cả",
          ),
          Tab(
            text: "Tin tức",
          ),
          Tab(
            text: "Ưu đãi",
          ),
          Tab(
            text: "Bí quyết sống",
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        indicator: new BubbleTabIndicator(
          indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
          indicatorColor: Colors.blue[100],
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 500,
        ),
      ),
    );
  }
}
