import 'package:base_code_project/app/auth_bloc/bloc.dart';
import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/local/barrel_local.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/router.dart';
import 'package:base_code_project/presentation/screen/menu/appointment/sc_appointment.dart';
import 'package:base_code_project/presentation/screen/menu/home/sc_home.dart';
import 'package:base_code_project/presentation/screen/menu/notification/sc_notification.dart';
import 'package:base_code_project/presentation/screen/menu/profile/sc_profile.dart';
import 'package:base_code_project/presentation/screen/menu/social/sc_social.dart';
import 'package:base_code_project/presentation/screen/service_price/sc_servicePrice.dart';
import 'package:base_code_project/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../../../app/constants/barrel_constants.dart';

class TabNavigatorRoutes {
  static const String home = '/home';
  static const String news = '/news';
  static const String comming = '/comming';
  static const String notification = '/notification';
  static const String account = '/account';
}

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  PageController _pageController;

  List<FABBottomNavItem> _navMenus = List();
  int _selectedIndex = 0;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  List<FABBottomNavItem> _getTab() {
    return List.from([
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.home,
          tabItem: TabItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/home.png'),
      FABBottomNavItem.asset(
        route: TabNavigatorRoutes.news,
        tabItem: TabItem.news,
        navigatorKey: GlobalKey<NavigatorState>(),
        assetUri: 'assets/images/img_term_and_policy.png',
      ),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.comming,
          tabItem: TabItem.comming,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/social-care.png'),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.notification,
          tabItem: TabItem.notification,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/nav_bell.png'),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.account,
          tabItem: TabItem.account,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/nav_user.png'),
    ]);
  }

  // goToPage({int page}) {
  //   WithAuth.isAuth(ifNotAuth: () {
  //     if (page == 3 || page == 4) {
  //       AppNavigator.navigateLogin();
  //     } else {
  //       if (page != _selectedIndex) {
  //         setState(() {
  //           this._selectedIndex = page;
  //         });
  //         _pageController.jumpToPage(_selectedIndex);
  //       }
  //     }
  //   }, ifAuth: () {
  //     if (page != _selectedIndex) {
  //
  //     }
  //   });
  // }
  goToPage({int page}) {
    setState(() {
      this._selectedIndex = page;
    });
    _pageController.jumpToPage(_selectedIndex);
  }

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) async {
        if (state is Authenticated) {
          //  BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
          Navigator.of(context)
              .pushNamedAndRemoveUntil(BaseRouter.NAVIGATION, (route) => false);
          Future.delayed(Duration(seconds: 2), () async {
            final prefs = LocalPref();
            bool isFirst = await prefs.getBool(AppPreferences.new_register);
            if (isFirst) {
              prefs.saveBool(AppPreferences.new_register, false);
              GetDialogUtils.createNotify(
                  message: AppLocalizations.of(Get.context)
                      .translate('register_verify.success_message'),
                  positiveLabel: AppLocalizations.of(Get.context)
                      .translate('register_verify.success_positive'),
                  negativeLabel: AppLocalizations.of(Get.context)
                      .translate('register_verify.success_negative'),
                  onPositiveTap: () {
                    //    AppNavigator.navigateVoucher();
                  });
            }
          });
        }
        if (state is Unauthenticated) {
          Navigator.of(context)
              .pushNamedAndRemoveUntil(BaseRouter.NAVIGATION, (route) => false);
        }
      },
      child: Scaffold(
          key: _drawerKey,
          // drawer: Container(
          //   color: AppColor.WHITE,
          //   width: MediaQuery.of(context).size.width * 2.5 / 10,
          //   height: double.infinity,
          //   child: WidgetCategoriesVerticalDrawer(),
          // ),
          bottomNavigationBar: WidgetFABBottomNav(
            height: 50,
            centerItemText: AppString.HOME,
            backgroundColor: AppColor.WHITE,
            selectedIndex: _selectedIndex,
            onTabSelected: (index) async {
              goToPage(page: index);
            },
            items: _navMenus,
            selectedColor: AppColor.NAV_ITEM_SELECTED_COLOR,
            color: AppColor.NAV_ITEM_COLOR,
          ),
          body: PageView(
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
            onPageChanged: (newPage) {
              setState(() {
                this._selectedIndex = newPage;
              });
            },
            children: [
              HomeScreen(
                drawer: _drawerKey,
                action: goToPage,
              ),
              AppointmentScreen(
                  //  drawer: _drawerKey,
                  ),
              SocialScreen(
                drawer: _drawerKey,
              ),
              NotificationScreen(
                  //  drawer: _drawerKey,
                  ),
              ProfileScreen(
                  //  drawer: _drawerKey,
                  ),
            ],
          )),
    );
  }
}
