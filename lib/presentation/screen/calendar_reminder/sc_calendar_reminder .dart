import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CalendarReminderScreen extends StatefulWidget {
  @override
  _CalendarReminderScreenState createState() => _CalendarReminderScreenState();
}

class _CalendarReminderScreenState extends State<CalendarReminderScreen> {
  String dropdownValue = "Khác";
  String dropdownValueTwo = '29/05/2020';

  String dropdownValueThree = '20:00';

  @override
  Widget build(BuildContext context) {
    // var userRepository = RepositoryProvider.of<UserRepository>(context);

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        // scrollDirection: Axis.vertical,
        child: ListView(
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.05,
                    color: Colors.white,
                  ),
                  Positioned.fill(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Icon(
                              Icons.arrow_back_ios_sharp,
                              size: 20,
                            ),
                          ),
                        ), // SizedBox(width: 90,),
                        Expanded(
                          flex: 7,
                          child: Text(
                            'Lịch nhắc',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                            flex: 2,
                            child: Container(
                              margin: EdgeInsets.only(right: 20),
                              child: Text(
                                "Lưu",
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.end,
                              ),
                            )),

                        //tensanpham
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "Loại nhắc nhở",
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    // height: 200,
                    // color: Colors.red,
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                      value: dropdownValue,
                      icon: Icon(Icons.keyboard_arrow_down),
                      iconSize: 24,
                      elevation: 16,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                      underline: Container(
                        height: 2,
                        color: Colors.black,
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;
                        });
                      },
                      items: <String>[
                        'Khác',
                        'Bình thường',
                        'Đặc biệt',
                        'Liên tục'
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    )),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 1,
                    color: Colors.grey[300],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: Text(
                      "Ngày nhắc",
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    // height: 200,
                    // color: Colors.red,
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                      value: dropdownValueTwo,
                      icon: Icon(Icons.keyboard_arrow_down),
                      iconSize: 24,
                      elevation: 16,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                      underline: Container(
                        height: 2,
                        color: Colors.black,
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValueTwo = newValue;
                        });
                      },
                      items: <String>[
                        '29/05/2020',
                        '06/12/2020',
                        '07/12/2020',
                        '08/12/2020'
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    )),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 1,
                    color: Colors.grey[300],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: Text(
                      "Loại nhắc nhở",
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    // height: 200,
                    // color: Colors.red,
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                      value: dropdownValueThree,
                      icon: Icon(Icons.keyboard_arrow_down),
                      iconSize: 24,
                      elevation: 16,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                      underline: Container(
                        height: 2,
                        color: Colors.black,
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValueThree = newValue;
                        });
                      },
                      items: <String>[
                        '20:00',
                        '21:00',
                        '22:00',
                        '23:00',
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    )),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 1,
                    color: Colors.grey[300],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: Text(
                      "Ghi chú",
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
// Container(
//   height: 200,
//   width: MediaQuery.of(context).size.width,
//
//   child: TextFormField(
//     // initialValue: 'Nhập nội dung ghi chú',
//     decoration: InputDecoration(
//       // labelText: 'Label text',
//       hintText:  'Nhập nội dung ghi chú',
//       hintStyle: TextStyle(color: Colors.grey[500]),
//       border: OutlineInputBorder(
//       ),
//     ),
//   ),
// ),
                  Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    child: Card(
                      // elevation: 4.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                          side: BorderSide(width: 1, color: Colors.grey[500])),
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        child: TextFormField(
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nhập nội dung ghi chú',
                            hintStyle: TextStyle(color: Colors.grey[500]),
                            hintMaxLines: 8,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
