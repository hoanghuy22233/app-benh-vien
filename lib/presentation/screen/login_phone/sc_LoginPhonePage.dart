import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/screen/login_phone/widget_button_post.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class LoginPhonePage extends StatefulWidget {
  LoginPhonePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPhonePage createState() => _LoginPhonePage();
}

class _LoginPhonePage extends State<LoginPhonePage> {
  bool _isButtonDisabled = true;
  @override
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Swiper(
            itemBuilder: (BuildContext context, int index) {
              return new Image.asset(
                "assets/images/firstBanner.png",
                fit: BoxFit.fill,
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
              );
            },
            pagination: new SwiperPagination(
              alignment: Alignment.center,
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 2.25),
              builder: new DotSwiperPaginationBuilder(
                  color: Colors.blue[300], activeColor: Colors.blue[100]),
            ),

            autoplay: false,
            itemCount: 3,
            scrollDirection: Axis.horizontal,
            // control: new SwiperControl(),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 4,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
            ),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width / 4),
                      child: Text('An tâm sống khoẻ cùng Việt Tiệp',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          )),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Expanded(
                    flex: 3,
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: _buildSubmitButton()),
                  ),
                  Expanded(
                    flex: 4,
                    child: Center(
                      child: Text(
                        'Hoặc kết nối tài khoản mạng xã hội',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.blue[300],
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
                ]),
          )
        ],
      ),
    ); // This trailing comma makes auto-formatting nicer for build methods.
  }

  _buildSubmitButton() {
    return WidgetButtonPost(
      onTap: () {
        _isButtonDisabled = false;
        AppNavigator.navigateEnterPhoneNumber();
      },
      isEnable: _isButtonDisabled,
      text: "Tham gia",
    );
  }
}
