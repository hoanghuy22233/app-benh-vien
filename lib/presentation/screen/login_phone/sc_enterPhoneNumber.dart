import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class EnterPhonePage extends StatefulWidget {
  EnterPhonePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _EnterPhonePage createState() => _EnterPhonePage();
}

class _EnterPhonePage extends State<EnterPhonePage> {
  String dropdownValue = '+84';
  bool showvalue = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          Align(
              alignment: Alignment.centerLeft,
              child: GestureDetector(
                onTap: () => AppNavigator.navigateBack(),
                child: Icon(
                  Icons.chevron_left,
                  size: 30,
                ),
              )),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 50),
            child: Text(
              "An tâm sống khoẻ cùng eVietTiep",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    height: 30,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.circular(3),
                    ),
                    child: Center(
                      child: DropdownButton<String>(
                        value: dropdownValue,
                        icon: Padding(
                          padding: EdgeInsets.only(left: 5),
                          child: Icon(Icons.keyboard_arrow_down),
                        ),
                        iconSize: 14,
                        elevation: 16,
                        style: TextStyle(color: Colors.black),
                        onChanged: (String newValue) {
                          setState(() {
                            dropdownValue = newValue;
                          });
                        },
                        items: <String>['+84', '+85', '+86', '+87']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                    flex: 8,
                    child: TextFormField(
                      enableInteractiveSelection: false,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Nhập số điện thoại của bạn",
                          hintStyle: TextStyle(color: Colors.grey[400])),
                    )),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Divider(
              color: Colors.grey[400],
              thickness: 0.5,
              height: 1,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              'Nếu tiếp tục, bạn sẽ nhận được tin nhắn SMS để xác minh',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.grey[400], fontSize: 14),
            ),
          ),
          Spacer(),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 50,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.white,
                  Colors.grey[300]
                ],
                tileMode: TileMode.repeated,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "Tôi đồng ý với ",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 12)),
                      TextSpan(
                          text: "Điều khoản sử dụng ",
                          style: TextStyle(
                              color: Colors.blue[300],
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold,
                              fontSize: 12)),
                      TextSpan(
                          text: "của eVietTiep ",
                          style: TextStyle(color: Colors.black, fontSize: 12)),
                    ]),
                  ),
                  Spacer(),
                  SizedBox(
                    height: 24,
                    width: 24,
                    child: Checkbox(

                      value: this.showvalue,
                      activeColor: Colors.blue[300],
                      onChanged: (bool value) {
                        setState(() {
                          this.showvalue = value;
                        });
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          showvalue==true?
          GestureDetector(
            onTap: ()=> AppNavigator.navigateNavigation(),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: Colors.blue[300],
                borderRadius: BorderRadius.circular(5),
              ),
              child: Center(
                child: Text(
                  'Tiếp tục',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ),
            ),
          ):Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: MediaQuery.of(context).size.width,
            height: 50,
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.circular(5),
            ),
            child: Center(
              child: Text(
                'Tiếp tục',
                style: TextStyle(color: Colors.blue, fontSize: 14),
              ),
            ),
          )
        ],
      ),
    );
  }
}
