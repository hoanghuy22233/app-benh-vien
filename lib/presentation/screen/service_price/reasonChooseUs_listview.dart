import 'package:base_code_project/model/enitity_offline/reasonChooseUs_item.dart';
import 'package:flutter/material.dart';

class ReasonChooseUsListView extends StatelessWidget {
  final int id;
  const ReasonChooseUsListView({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                flex: 2,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(500),
                          color: Colors.blue[100]),
                      child: Center(child: Image.asset(
                        '${reasonChooseUsItem[id].image}',
                        width: 30,
                        height: 30,
                      ),),
                    ),

                    reasonChooseUsItem[id].free == 1
                        ? Container(
                      width: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.red
                      ),
                      child: Center(
                        child: Text(
                          'Miễn Phí',
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      ),
                    )
                        : Container(
                      color: Colors.transparent,
                      child: null,
                    )
                  ],
                )),
            Expanded(
                flex: 8,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('${reasonChooseUsItem[id].title}', style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 14
                    ),),
                    SizedBox(height: 5,),
                    Text('${reasonChooseUsItem[id].detail}', style: TextStyle(
                      fontSize: 12,
                    ),),
                  ],
                )),
          ],
        ),
        SizedBox(height: 10,),
      ],
    );
  }
}
