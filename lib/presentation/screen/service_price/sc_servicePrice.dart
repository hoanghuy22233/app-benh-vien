import 'package:base_code_project/presentation/screen/service_price/widget_list_service.dart';
import 'package:base_code_project/presentation/screen/service_price/widget_service_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ServiceScreen extends StatefulWidget {
  @override
  _ServiceScreen createState() => _ServiceScreen();
}

class _ServiceScreen extends State<ServiceScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _buildAppBarService(),
          Expanded(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.blue[100],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 10,),
                  Icon(
                    Icons.location_pin,
                    size: 25  ,
                    color: Colors.white,
                  ),
                  SizedBox(width: 10,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Bạn đang xem dịch vụ tại:", style: TextStyle( color: Colors.white, fontSize: 12),),
                      Text('Bệnh Viện Việt Tiệp', style: TextStyle(color: Colors.white, fontSize: 14),),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Expanded(flex: 8, child: WidgetListService())
        ],
      ),
    ) ;
  }

  _buildAppBarService() => WidgetServiceAppbar();
}
