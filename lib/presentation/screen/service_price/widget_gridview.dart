import 'package:base_code_project/model/enitity_offline/all_service.dart';
import 'package:flutter/material.dart';
class OverviewGridView extends StatelessWidget {
  final int id;
  OverviewGridView({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return   Center(
      child: GridView.count(
        padding: EdgeInsets.symmetric(horizontal: 10),
        shrinkWrap: true,
        crossAxisCount: 2,
        physics: NeverScrollableScrollPhysics(),
        mainAxisSpacing: 1,
        childAspectRatio: 8,
        children: [
          Row(
            children: [
              Image.asset(
                'assets/images/ic_sex.png',
                width: 15,
                height: 15,
              ),
              SizedBox(
                width: 10,
              ),
              Text('${serviceItem[id].sex}', style: TextStyle(fontSize: 12),),
            ],
          ),
          Row(
            children: [
              Image.asset(
                'assets/images/flask.png',
                width: 15,
                height: 15,
              ),
              SizedBox(
                width: 10,
              ),
              Text('${serviceItem[id].xetnghiem}', style: TextStyle(fontSize: 12),),
            ],
          ),
          Row(
            children: [
              Image.asset(
                'assets/images/ic_birthdaycake.png',
                width: 15,
                height: 15,
              ),
              SizedBox(
                width: 10,
              ),
              Text('${serviceItem[id].age}', style: TextStyle(fontSize: 12),),
            ],
          ),
          serviceItem[id].muckham != null
              ? Row(
            children: [
              Image.asset(
                'assets/images/ic_muckham.png',
                width: 15,
                height: 15,
              ),
              SizedBox(
                width: 10,
              ),
              Text('${serviceItem[id].muckham}', style: TextStyle(fontSize: 12),),
            ],
          )
              : Container(
            height: 0,
            width: 0,
            child: null,
          )
        ],
      ),
    );
  }
}


