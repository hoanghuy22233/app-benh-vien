import 'package:base_code_project/model/enitity_offline/all_service.dart';
import 'package:base_code_project/model/enitity_offline/reasonChooseUs_item.dart';
import 'package:base_code_project/presentation/screen/service_price/reasonChooseUs_listview.dart';
import 'package:base_code_project/presentation/screen/service_price/widget_gridview.dart';
import 'package:base_code_project/presentation/screen/service_price/widget_priceAndBook.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailServiceScreen extends StatefulWidget {
  final int id;
  DetailServiceScreen({Key key, this.id}) : super(key: key);
  _DetailServiceScreen createState() => new _DetailServiceScreen();
}

class _DetailServiceScreen extends State<DetailServiceScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(children: [
      Container(
        height: MediaQuery.of(context).size.height / 3,
        child: Stack(
          alignment: Alignment.topLeft,
          children: [
            Image.asset(
              '${serviceItem[widget.id].image}',
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 3,
              fit: BoxFit.cover,
            ),
            Positioned(
              top: 20,
              child: GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.chevron_left,
                    color: Colors.white,
                    size: 40,
                  )),
            ),
          ],
        ),
      ),
      Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10,
            ),
            Text(
              '${serviceItem[widget.id].title}',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              '${serviceItem[widget.id].describe}',
              style: TextStyle(fontSize: 14, height: 1.5),
            ),
            SizedBox(
              height: 5,
            ),
            Divider(
              color: Colors.grey[300],
              thickness: 0.5,
            ),
            OverviewGridView(
              id: widget.id,
            ),
            Divider(
              color: Colors.grey[300],
              thickness: 0.5,
            ),
            SizedBox(
              height: 5,
            ),
            _buildPriceAndBook(),
            SizedBox(
              height: 5,
            ),
            Divider(
              color: Colors.grey[300],
              thickness: 0.5,
            ),
            Text(
              'Tại sao nên đặt gói khám và xét nghiệm tại nhà của chúng tôi ',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            Container(
              height: 230,
              child: ListView.builder(
                  padding: EdgeInsets.only(top: 10),
                  physics: NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: reasonChooseUsItem.length,
                  itemBuilder: (context, index) {
                    return Container(
                      child: ReasonChooseUsListView(
                        id: index,
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    ])) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  _buildPriceAndBook() => PriceAndBook(
        id: widget.id,
      );
}
