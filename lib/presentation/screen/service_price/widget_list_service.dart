import 'package:base_code_project/model/enitity_offline/all_service.dart';
import 'package:base_code_project/presentation/screen/service_price/service_listview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetListService extends StatelessWidget {
  @override
  final int id;
  WidgetListService({Key key, this.id}) : super(key: key);
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        padding: EdgeInsets.zero,
        scrollDirection: Axis.vertical,
        itemCount: serviceItem.length,
        itemBuilder: (context, index) {
          return Container(
              margin: EdgeInsets.only(bottom: 10),
              child: ServiceListView(
                id: index,
              ));
        },
      ),
    );
  }
}
