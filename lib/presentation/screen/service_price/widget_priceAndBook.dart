import 'package:base_code_project/model/enitity_offline/all_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PriceAndBook extends StatelessWidget {
  final int id;

  const PriceAndBook({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween ,
      children: [
        if (serviceItem[id].isDiscount == 1) Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  '${serviceItem[id].priceSale} ',
                  style: TextStyle(
                    color: Colors.grey,
                    decoration: TextDecoration.lineThrough,
                  ),
                ),
                SizedBox(
                  width: 7,
                ),
                Text(
                  '${serviceItem[id].discount}',
                  style: TextStyle(color: Colors.green, fontSize: 16),
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              '${serviceItem[id].price}',
              style: TextStyle(color: Colors.red, fontSize: 16),
            )
          ],
        ) else Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Giá dịch vụ",
              style: TextStyle(color: Colors.grey, fontSize: 14),
            ),
            SizedBox(
              height: 5,
            ),
            Text('${serviceItem[id].price}', style: TextStyle(color: Colors.red, fontSize: 16)),
          ],
        ),
        Container(
            width: 160,
            height: 40,
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Center(
              child: Text('Đặt lịch hẹn', style: TextStyle(
                  color: Colors.white, fontSize: 14
              ),),
            )
        )
      ],
    );
  }
}
