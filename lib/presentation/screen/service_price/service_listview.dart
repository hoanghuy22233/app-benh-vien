import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/model/enitity_offline/all_service.dart';
import 'package:base_code_project/presentation/screen/service_price/widget_gridview.dart';
import 'package:base_code_project/presentation/screen/service_price/widget_priceAndBook.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'detail_service_screen.dart';

class ServiceListView extends StatelessWidget {
  final int id;
  ServiceListView({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 15,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(
                    "${serviceItem[id].image}",
                    width: 100,
                    height: 75,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                  flex: 6,
                  child: GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailServiceScreen(
                                  id: id,
                                ))),
                    child: Container(
                      child: Stack(
                        alignment: Alignment.bottomRight,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                serviceItem[id].title.length <= 30
                                    ? serviceItem[id].title
                                    : serviceItem[id].title.substring(0, 30) +
                                        '...',
                                style: AppStyle.DEFAULT_SMALL.copyWith(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 14),
                                textAlign: TextAlign.start,
                                maxLines: 1,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "${serviceItem[id].describe}",
                                overflow: TextOverflow.clip,
                                maxLines: 3,
                                style: TextStyle(color: Colors.grey),
                              )
                            ],
                          ),
                          Container(
                              color: Colors.grey[50],
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'Xem tiếp',
                                    style: TextStyle(
                                        color: Colors.blue[300], fontSize: 14),
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    size: 10,
                                    color: Colors.blue[300],
                                  )
                                ],
                              )),
                        ],
                      ),
                    ),
                  )),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
            margin: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.grey[200]),
            height: 50,
            width: MediaQuery.of(context).size.width,
            child: OverviewGridView(
              id: id,
            )),
        SizedBox(
          height: 10,
        ),
        Padding(
          child: PriceAndBook(
            id: id,
          ),
          padding: EdgeInsets.symmetric(horizontal: 5),
        ),
        SizedBox(
          height: 15,
        ),
        Divider(
          color: Colors.grey,
          thickness: 0.5,
          height: 0,
        )
      ],
    );
  }
}
