import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/repo/user_repository.dart';
import 'package:base_code_project/presentation/common_widgets/widget_circle_progress.dart';
import 'package:base_code_project/presentation/common_widgets/widget_logo.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    openLogin();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  }
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [_buildLogo(), _buildProgress()],
        ),
      ),
    );
  }

  _buildLogo() => WidgetLogo(
        height: Get.width * 0.63,
        widthPercent: 0.63,
      );

  _buildProgress() => WidgetCircleProgress();

  void openLogin() async {
    Future.delayed(Duration(seconds: 2), () {
      AppNavigator.navigateLogin();
    });
  }
}
