import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/history_question/widget_history_question_appbar.dart';
import 'package:flutter/material.dart';

class HistoryQuestionScreen extends StatefulWidget {
  @override
  _HistoryQuestionScreenState createState() => _HistoryQuestionScreenState();
}

class _HistoryQuestionScreenState extends State<HistoryQuestionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        _appbar(),
        Expanded(
            child: ListView(
          padding: EdgeInsets.all(10),
          scrollDirection: Axis.vertical,
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 6,
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Card(
                        color: Colors.blue[50],
                        child: Container(
                          margin: EdgeInsets.only(top: 15, left: 15, right: 15),
                          //padding: EdgeInsets.all(5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 25,
                                width: 25,
                                padding: EdgeInsets.all(5.0),
                                decoration: BoxDecoration(
                                    color: Colors.blue, shape: BoxShape.circle),
                                child: Image.asset(
                                  "assets/images/question.png",
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text("Câu hỏi đang chờ",
                                  style: TextStyle(fontSize: 14))
                            ],
                          ),
                        )),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Card(
                        color: Colors.blue[50],
                        child: Container(
                          margin: EdgeInsets.only(top: 15, left: 15, right: 15),
                          //padding: EdgeInsets.all(5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 25,
                                width: 25,
                                padding: EdgeInsets.all(5.0),
                                decoration: BoxDecoration(
                                    color: Colors.blue, shape: BoxShape.circle),
                                child: Image.asset(
                                  "assets/images/question.png",
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text("Câu hỏi bị từ chối",
                                  style: TextStyle(fontSize: 14))
                            ],
                          ),
                        )),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.55,
              decoration: BoxDecoration(color: Colors.white),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Câu hỏi đã được trả lời",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                      child: Container(
                    height: 200,
                    width: 200,
                    child: Image.asset("assets/images/question_background.png"),
                  )),
                  Center(
                    child: Text(
                      "Bạn chưa có câu hỏi nào",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                    ),
                  ),
                  Center(
                    child: Text(
                      "Đặt câu hỏi để bác sĩ giải quyết vấn đề của bạn tốt nhất",
                      style: TextStyle(fontSize: 14),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 20,
            ),
            Container(
              color: Colors.white,
              //height: MediaQuery.of(context).size.height * 0.8,
              child: _buttonQuestion(),
            ),
          ],
        )),
      ],
    ));
  }

  Widget _appbar() => WidgetDetailAppbar(titleText: "Lịch sử câu hỏi");

  Widget _buttonQuestion() => WidgetLoginButton(
        onTap: () {},
        text: "Hỏi Bác sĩ miễn phí",
        isEnable: true,
      );
}
