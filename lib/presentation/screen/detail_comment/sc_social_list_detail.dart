import 'package:base_code_project/model/enitity_offline/askDoctor.dart';
import 'package:base_code_project/presentation/common_widgets/widget_text_input.dart';
import 'package:base_code_project/presentation/screen/detail_comment/widget_socail_list_detail_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewsDetailNewsPage extends StatefulWidget {
  final int id;
  NewsDetailNewsPage({Key key, this.id}) : super(key: key);
  _NewsDetailNewsPageState createState() => new _NewsDetailNewsPageState();
}

class _NewsDetailNewsPageState extends State<NewsDetailNewsPage> {
  bool isReadDetail = false;
  String textCategory =
      "Chào bạn, cảm ơn bạn đã gửi câu hỏi. Bạn cần có chế độ ăn uống. Vận động và nghỉ ngơi hợp lý. bạn có thể gặp bác sĩ chuyên khoa cơ xương khớp để biết rõ hơn";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          WidgetNewsDetailAppbar(),
          Expanded(
              child: ListView(
            padding: EdgeInsets.zero,
            scrollDirection: Axis.vertical,
            children: [
              Container(
                color: Colors.grey[100],
                child: Column(
                  children: [
                    Card(
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: <Widget>[
                                SizedBox(
                                  width: 20,
                                ),
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(500),
                                  child: Image.asset(
                                    askAndAsnwerItem[widget.id].avatarDoctor,
                                    height: 40,
                                    // fit: BoxFit.fill,
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  // flex: 8,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Text(
                                              askAndAsnwerItem[widget.id]
                                                  .nameDoctor,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        askAndAsnwerItem[widget.id].time,
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 5),
                              child: Container(
                                height: isReadDetail ? null : 50,
                                child: Text(
                                  askAndAsnwerItem[widget.id].question,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            askAndAsnwerItem[widget.id].question.length > 100
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            isReadDetail = !isReadDetail;
                                          });
                                        },
                                        child: isReadDetail
                                            ? Text(
                                                "Thu gọn",
                                                style: TextStyle(
                                                    color: Colors.blue),
                                              )
                                            : Text(
                                                "Xem thêm",
                                                style: TextStyle(
                                                    color: Colors.blue),
                                              ),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      )
                                    ],
                                  )
                                : SizedBox(),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    askAndAsnwerItem[widget.id].sex,
                                    style: TextStyle(color: Colors.grey),
                                  )),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    width: 50,
                                  ),
                                  Text(
                                      "${askAndAsnwerItem[widget.id].share} chia sẻ"),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Image.asset(
                                    "assets/images/img_share_color.png",
                                    height: 15,
                                    width: 15,
                                  ),
                                  SizedBox(
                                    width: 45,
                                  ),
                                  Text(
                                      "${askAndAsnwerItem[widget.id].likeCount} lượt thích"),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  askAndAsnwerItem[widget.id].like == 0
                                      ? Image.asset(
                                          "assets/icons/ic_heart_gray.png",
                                          height: 15,
                                          width: 15,
                                        )
                                      : Image.asset(
                                          "assets/icons/ic_heart_red.png",
                                          height: 15,
                                          width: 15,
                                        ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 1,
                      color: Colors.grey[100],
                      margin:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: <Widget>[
                              SizedBox(
                                width: 20,
                              ),
                              ClipRRect(
                                borderRadius: BorderRadius.circular(500),
                                child: Image.asset(
                                  "assets/images/avatar_doctor.png",
                                  height: 40,
                                  // fit: BoxFit.fill,
                                ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                // flex: 8,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            "BS. Lại Xuân Lợi",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      "2 giờ trước",
                                      style: TextStyle(fontSize: 12),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 5),
                            child: Container(
                              height: isReadDetail ? null : 50,
                              child: Text(
                                askAndAsnwerItem[widget.id].answer,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          askAndAsnwerItem[widget.id].answer.length > 100
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          isReadDetail = !isReadDetail;
                                        });
                                      },
                                      child: isReadDetail
                                          ? Text(
                                              "Thu gọn",
                                              style: TextStyle(
                                                  color: Colors.blue,
                                                  fontSize: 12),
                                            )
                                          : Text(
                                              "Xem thêm",
                                              style: TextStyle(
                                                  color: Colors.blue,
                                                  fontSize: 12),
                                            ),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    )
                                  ],
                                )
                              : SizedBox(),
                          SizedBox(
                            height: 5,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              _buildBottom()
            ],
          ))
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  _buildBottom() {
    return Container(
        color: Colors.grey[300],
        height: MediaQuery.of(context).size.height * 0.07,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: Icon(Icons.attachment),
                )),
            Expanded(
              flex: 8,
              child: _buildTextFieldChat(),
            ),
            Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: GestureDetector(
                    onTap: () {},
                    child: Icon(Icons.send),
                  ),
                )),
          ],
        ));
  }

  _buildTextFieldChat() {
    return WidgetTextInput(
      inputType: TextInputType.emailAddress,
      //autovalidate: autoValidate,
      inputController: null,
      onChanged: (value) {},

      hint: "Trả lời",
    );
  }
}
