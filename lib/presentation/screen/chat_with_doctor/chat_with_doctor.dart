import 'package:base_code_project/app/constants/string/validator.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/model/enitity_offline/listDoctorAdvisory.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:base_code_project/presentation/common_widgets/widget_text_input.dart';
import 'package:base_code_project/presentation/screen/chat_with_doctor/widget_chat_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_1.dart';

class ChatWithDoctorScreen extends StatefulWidget {
  final int id;

  const ChatWithDoctorScreen({Key key, this.id}) : super(key: key);
  @override
  _ChatWithDoctorScreenState createState() => _ChatWithDoctorScreenState();
}

class _ChatWithDoctorScreenState extends State<ChatWithDoctorScreen> {

  TextEditingController _chatTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            _appbar(),
            Expanded(
              flex: 7,
              child: Container(
              height: MediaQuery.of(context).size.height*0.8,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                children: <Widget>[
                  ChatBubble(
                    clipper: ChatBubbleClipper1(type: BubbleType.sendBubble),
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.only(top: 20),
                    backGroundColor: Colors.blue,
                    child: Container(
                      padding: EdgeInsets.all(3),
                      constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width * 0.7,
                      ),
                      child: Text(
                        "Chào bác sĩ, tôi năm nay 20 tuổi. Tôi có triệu chứng đau đầu phía sau gáy, thường là vào buổi trưa. Bác sĩ có thể tư vấn cho tôi nên làm gì không?",
                        style: TextStyle(color: Colors.white,),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ),
                  ChatBubble(
                    clipper: ChatBubbleClipper1(type: BubbleType.receiverBubble),
                    backGroundColor: Color(0xffE7E7ED),
                    margin: EdgeInsets.only(top: 20),
                    child: Container(
                      padding: EdgeInsets.all(3),
                      constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width * 0.7,
                      ),
                      child: Text(
                        "Bạn nên đến bệnh viện để chúng tôi kiểm tra chi tiết hơn. Bạn có thể chụp X-quang và chụp CT để kiểm tra phần đầu của bạn.",
                        style: TextStyle(color: Colors.black),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ),
                  ChatBubble(
                    clipper: ChatBubbleClipper1(type: BubbleType.sendBubble),
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.only(top: 20),
                    backGroundColor: Colors.blue,
                    child: Container(
                      padding: EdgeInsets.all(3),
                      constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width * 0.7,
                      ),
                      child: Text(
                        "Chào bác sĩ, tôi năm nay 20 tuổi. Tôi có triệu chứng đau đầu phía sau gáy, thường là vào buổi trưa. Bác sĩ có thể tư vấn cho tôi nên làm gì không?",
                        style: TextStyle(color: Colors.white,),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ),
                  ChatBubble(
                    clipper: ChatBubbleClipper1(type: BubbleType.receiverBubble),
                    backGroundColor: Color(0xffE7E7ED),
                    margin: EdgeInsets.only(top: 20),
                    child: Container(
                      padding: EdgeInsets.all(3),
                      constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width * 0.7,
                      ),
                      child: Text(
                        "Bạn nên đến bệnh viện để chúng tôi kiểm tra chi tiết hơn. Bạn có thể chụp X-quang và chụp CT để kiểm tra phần đầu của bạn.",
                        style: TextStyle(color: Colors.black),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ),
                  ChatBubble(
                    clipper: ChatBubbleClipper1(type: BubbleType.sendBubble),
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.only(top: 20),
                    backGroundColor: Colors.blue,
                    child: Container(
                      padding: EdgeInsets.all(3),
                      constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width * 0.7,
                      ),
                      child: Text(
                        "Chào bác sĩ, tôi năm nay 20 tuổi. Tôi có triệu chứng đau đầu phía sau gáy, thường là vào buổi trưa. Bác sĩ có thể tư vấn cho tôi nên làm gì không?",
                        style: TextStyle(color: Colors.white,),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ),
                  ChatBubble(
                    clipper: ChatBubbleClipper1(type: BubbleType.receiverBubble),
                    backGroundColor: Color(0xffE7E7ED),
                    margin: EdgeInsets.only(top: 20),
                    child: Container(
                      padding: EdgeInsets.all(3),
                      constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width * 0.7,
                      ),
                      child: Text(
                        "Bạn nên đến bệnh viện để chúng tôi kiểm tra chi tiết hơn. Bạn có thể chụp X-quang và chụp CT để kiểm tra phần đầu của bạn.",
                        style: TextStyle(color: Colors.black),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  )
                ],
              ),
            )),
            Expanded(
                flex: 1,
                child: _buildBottom()),

          ],
        ),
    );
  }

  Widget _rightAvatar(){
    return Container(
      height: AppValue.ACTION_BAR_HEIGHT-10,
      width: AppValue.ACTION_BAR_HEIGHT-10,
      //padding: EdgeInsets.all(5.0),
      margin: EdgeInsets.only(right: 5.0),
      decoration: BoxDecoration(
        color: Colors.amber,
        shape: BoxShape.circle,
        image: DecorationImage(
          image: AssetImage("${doctorList[widget.id].avatar}"),
          fit: BoxFit.cover,
        ),
      ),

    );
  }

  Widget _appbar() => WidgetChatAppbar(
    left: [WidgetAppbarMenuBack()],
    title: "${doctorList[widget.id].name}",
    status: "Đang online",
    isOnline: true,
    right: [_rightAvatar()],
  );

  _buildTextFieldChat() {
    return WidgetTextInput(
      inputType: TextInputType.emailAddress,
      //autovalidate: autoValidate,
      inputController: _chatTextController,
      onChanged: (value) {
      },

      hint: "Nhập câu hỏi",
    );
  }

  _buildBottom(){
    return Container(
        height: MediaQuery.of(context).size.height*0.07,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: Icon(Icons.attachment),
                )
            ),
            Expanded(
              flex: 8,
              child: _buildTextFieldChat(),
            ),
            Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: GestureDetector(
                    onTap: (){
                    },
                    child: Icon(Icons.send),
                  ),
                )
            ),
          ],
        )
    );
  }

}
