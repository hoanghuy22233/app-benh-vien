import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:flutter/material.dart';

class WidgetChatAppbar extends StatefulWidget {
  final String title;
  final String status;
  final List<Widget> left;
  final List<Widget> right;
  final Color indicatorColor;
  final bool hasIndicator;
  final bool isOnline;

  WidgetChatAppbar(
      {Key key,
      this.title,
      this.status,
      this.left,
      this.right,
      this.indicatorColor,
      this.isOnline ,
      this.hasIndicator = false})
      : super(key: key);

  @override
  _WidgetChatAppbarState createState() => _WidgetChatAppbarState();
}

class _WidgetChatAppbarState extends State<WidgetChatAppbar> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          color: Colors.white,
          height: AppValue.ACTION_BAR_HEIGHT*1.1,
          margin: EdgeInsets.only(top: 20),
          child: Stack(
            children: [
              widget.left != null
                  ? Positioned.fill(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: widget.left,
                      ),
                    )
                  : SizedBox(),
              widget.right != null
                  ? Positioned.fill(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: widget.right,
                      ),
                    )
                  : SizedBox(),
              widget.title != null
                  ? Positioned.fill(
                      child: FractionallySizedBox(
                          widthFactor: .8,
                          child: Container(
                            margin: EdgeInsets.only(top: 5),
                            padding: const EdgeInsets.all(4.0),
                            child: Stack(
                              children: [
                                Container(
                                  height: AppValue.ACTION_BAR_HEIGHT * 1.25,
                                  child: Center(
                                    child: Column(
                                      children: [
                                        Text(
                                          widget.title,
                                          style: AppStyle.DEFAULT_MEDIUM.copyWith(
                                              color: AppColor.PRIMARY,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          widget.status,
                                          style: widget.isOnline ? AppStyle.DEFAULT_MEDIUM.copyWith(
                                              color: Colors.green, fontSize: 12) :
                                            AppStyle.DEFAULT_MEDIUM.copyWith(
                                              color: AppColor.RED, fontSize: 12),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                    )
                  : SizedBox(),
            ],
          ),
        ),
        widget.hasIndicator
            ? Divider(
                height: 1,
                thickness: 1,
                color: widget.indicatorColor != null
                    ? widget.indicatorColor
                    : Colors.grey,
              )
            : WidgetSpacer(
                height: 0,
              )
      ],
    );
  }
}
