import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:flutter/material.dart';

class WidgetSocialButton extends StatelessWidget {
  final Widget image;
  final String text;
  final Function onTap;

  const WidgetSocialButton({this.image, this.text, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Column(
          children: [
            FractionallySizedBox(
              widthFactor: .4,
              child: image,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(text, style: AppStyle.DEFAULT_SMALL,),
            )
          ],
        ),
      ),
    );
  }
}
