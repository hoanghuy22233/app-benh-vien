import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/utils/utils.dart';
import 'package:flutter/material.dart';

class WidgetBottomRegister extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 24),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Text(
                AppLocalizations.of(context).translate('login.no_account'),
                style: TextStyle(color: Colors.grey[300], fontSize: 14)),
          ),
          Flexible(
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateRegisterPage();
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                child: WidgetLink(
                  text:
                      AppLocalizations.of(context).translate('login.register'),
                  //    onTap: AppNavigator.navigateRegister,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
