import 'package:base_code_project/app/auth_bloc/authentication_bloc.dart';
import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/repo/barrel_repo.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/login/barrel_login.dart';
import 'package:base_code_project/presentation/screen/login/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: BlocProvider(
          create: (context) => LoginBloc(
              userRepository: userRepository,
              authenticationBloc: BlocProvider.of<AuthenticationBloc>(context)),
          child: Container(
              color: AppColor.PRIMARY_BACKGROUND,
              child: ListView(
                children: [
                  WidgetSpacer(
                    height: 20,
                  ),
                  _buildImage(),
                  _buildLoginForm(),
                  _buildBottomRegister(),
                  WidgetSpacer(
                    height: 20,
                  ),
                ],
              )),
        ),
      ),
    );
  }

  _buildImage() => WidgetLoginLogo();

  _buildLoginForm() => WidgetLoginForm();

  _buildBottomRegister() => WidgetBottomRegister();
}
