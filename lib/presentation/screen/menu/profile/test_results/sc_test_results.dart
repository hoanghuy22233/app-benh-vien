import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/history_question/widget_history_question_appbar.dart';
import 'package:base_code_project/presentation/screen/menu/profile/test_results/widget_test_result_appbar.dart';
import 'package:flutter/material.dart';


class TestResultScreen extends StatefulWidget {
  @override
  _TestResultScreenState createState() => _TestResultScreenState();
}

class _TestResultScreenState extends State<TestResultScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _appbar(),
          Expanded(
              child: ListView(
                padding: EdgeInsets.all(10),
                scrollDirection: Axis.vertical,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.45,
                    decoration: BoxDecoration(
                        color: Colors.white
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        _processAppbar(),
                        _buildProcess(),
                      ],
                    ),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.5,
                    decoration: BoxDecoration(
                        color: Colors.white
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        _testAppbar(),
                        _buildTest(),
                      ],
                    ),
                  ),
                ],
              )
          ),
          _buildBottom()
        ],
      ),
    );
  }

  Widget _appbar() => WidgetTestResultAppbar(
    left: [WidgetAppbarMenuBack()],
    right: [_rightAppbar()],
  );

  Widget _rightAppbar(){
    return Container(
      height: 25,
      width: 25,
      decoration: BoxDecoration(
          color: Colors.grey,
          shape: BoxShape.circle
      ),
      child: Icon(Icons.add, color: Colors.white,),
    );
  }

  Widget _processAppbar() => WidgetAppbar(
    left: [Text("Quy trình khám và xét nghiệm", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),)],
  );

  Widget _buildProcess(){
    return Container(
        height: MediaQuery.of(context).size.height *0.35,
        child: Container(
          //height: MediaQuery.of(context).size.height*0.5,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              Card(
                  color: Colors.blue[50],
                  child: Container(
                    width: 180,
                    padding: EdgeInsets.all(5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 30,
                          width: 100,
                          margin: EdgeInsets.all(5.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Bước 1", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 50,
                              width: 50,
                              //padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: Colors.blue[200],
                                shape: BoxShape.circle,
                              ),
                              child: Image.asset("assets/images/houses.png"),
                            ),
                            SizedBox(height: 20,),
                            Text("Điều dưỡng Bệnh viện Tiệp sẽ đến tận nhà để lấy mẫu xét nghiệm và chuyển đến bệnh viện để phân tích.", style: TextStyle(fontSize: 14), textAlign: TextAlign.center,),
                          ],
                        )
                      ],
                    ),
                  )
              ),
              Card(
                  color: Colors.blue[50],
                  child: Container(
                    width: 180,
                    padding: EdgeInsets.all(5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 30,
                          width: 100,
                          margin: EdgeInsets.all(5.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Bước 2", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 50,
                              width: 50,
                              //padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: Colors.blue[200],
                                shape: BoxShape.circle,
                              ),
                              child: Image.asset("assets/images/globe.png"),
                            ),
                            SizedBox(height: 20,),
                            Text("Kết quả phân tích của bệnh nhân sẽ được nhận tại website http://viettiephospital.com.vn.", style: TextStyle(fontSize: 14), textAlign: TextAlign.center,),
                          ],
                        )
                      ],
                    ),
                  )
              ),
              Card(
                  color: Colors.blue[50],
                  child: Container(
                    width: 180,
                    padding: EdgeInsets.all(5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 30,
                          width: 100,
                          margin: EdgeInsets.all(5.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Bước 3", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 50,
                              width: 50,
                              //padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: Colors.blue[200],
                                shape: BoxShape.circle,
                              ),
                              child: Image.asset("assets/images/houses.png"),
                            ),
                            SizedBox(height: 20,),
                            Text("Điều dưỡng Bệnh viện Tiệp sẽ đến tận nhà để lấy mẫu xét nghiệm và chuyển đến bệnh viện để phân tích.", style: TextStyle(fontSize: 14), textAlign: TextAlign.center,),
                          ],
                        )
                      ],
                    ),
                  )
              ),
            ],
          ),
        ),
    );
  }

  Widget _testAppbar() => WidgetAppbar(
    left: [Text("Hạng mục xét nghiệm (13)", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),)],
  );

  Widget _buildTest(){
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      child: Container(
        //height: MediaQuery.of(context).size.height*0.5,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          scrollDirection: Axis.vertical,
          children: <Widget>[
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black12,
            ),
            Text("Urea", style: TextStyle(fontSize: 16),),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black12,
            ),
            Text("Glucose", style: TextStyle(fontSize: 16),),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black12,
            ),
            Text("Creatinine", style: TextStyle(fontSize: 16),),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black12,
            ),
            Text("GGT", style: TextStyle(fontSize: 16),),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black12,
            ),
            Text("Trylycerid", style: TextStyle(fontSize: 16),),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black12,
            ),
            Text("Cholestorol", style: TextStyle(fontSize: 16),),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black12,
            ),
            Text("Virus HIV", style: TextStyle(fontSize: 16),),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black12,
            ),
            Text("Máu", style: TextStyle(fontSize: 16),),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black12,
            ),
          ],
        ),
      ),
    );
  }

  _buildBottom(){
    return Container(
        height: MediaQuery.of(context).size.height*0.07,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          color: Colors.white
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
                flex: 2,
                child: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      shape: BoxShape.circle
                  ),
                  child: Icon(Icons.phone, color: Colors.white,),
                )
            ),
            Expanded(
              flex: 8,
              child: WidgetLoginButton(
                  onTap: (){
                  },
                  text: "Đặt lịch hẹn",
                  isEnable: true
              ),
            ),

          ],
        )
    );
  }
}
