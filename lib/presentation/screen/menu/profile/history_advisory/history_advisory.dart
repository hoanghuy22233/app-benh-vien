import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/chat_with_doctor/chat_with_doctor.dart';
import 'package:base_code_project/presentation/screen/history_question/widget_history_question_appbar.dart';
import 'package:base_code_project/model/enitity_offline/listDoctorAdvisory.dart';
import 'package:base_code_project/presentation/screen/menu/profile/history_advisory/widget_right_button.dart';
import 'package:flutter/material.dart';


class HistoryAdvisoryScreen extends StatefulWidget {
  @override
  _HistoryAdvisoryScreenState createState() => _HistoryAdvisoryScreenState();
}

class _HistoryAdvisoryScreenState extends State<HistoryAdvisoryScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            _appbar(),
            Expanded(
                child: ListView(
                  padding: EdgeInsets.all(10),
                  scrollDirection: Axis.vertical,
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.50,
                      decoration: BoxDecoration(
                          color: Colors.white
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Center(
                              child: Container(
                                height: 200,
                                width: 200,
                                child: Image.asset("assets/images/advisory_background.png"),
                              )
                          ),
                          Center(
                            child: Text("Chưa có trò chuyện nào với Bác sĩ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),),
                          ),
                          Center(
                            child: Text("Chọn bác sĩ phù hợp để được chăm sóc và tư vấn cho sức khỏe của bạn", style: TextStyle(fontSize: 14),textAlign: TextAlign.center,),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 20,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.42,
                      decoration: BoxDecoration(
                          color: Colors.white
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          _doctorAppbar(),
                          _buildListDoctor(),
                        ],
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      //height: MediaQuery.of(context).size.height * 0.8,
                      child: _buttonListDoctor(),
                    )
                  ],
                )
            ),

          ],
        )
    );
  }

  Widget _appbar() => WidgetDetailAppbar(titleText: "Lịch sử tư vấn");

  Widget _doctorAppbar() => WidgetAppbar(
    left: [Text("Bác sĩ tư vấn tích cực", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)],
    right: [WidgetRightButton(
      onTap: (){

      },
      text: "Xem thêm >",
      isEnable: true,
    )],
  );

  Widget _buttonListDoctor() => WidgetLoginButton(
    onTap: (){
      AppNavigator.navigateListDoctor();
    },
    text: "Xem danh sách Bác sĩ     >",
    isEnable: true,
  );

  Widget _buildListDoctor(){
    return Container(
        height: MediaQuery.of(context).size.height / 3,
        child: ListView.builder(
          padding: EdgeInsets.zero,
          scrollDirection: Axis.horizontal,
          itemCount: doctorList.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ChatWithDoctorScreen(id: index)
                ),
              ),
              child: Card(
                  color: Colors.blue[50],
                  child: Container(
                    width: 180,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 30,
                          width: 60,
                          //padding: EdgeInsets.all(5.0),
                          decoration: BoxDecoration(
                            color: Colors.amber,
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.star, color: Colors.white,),
                              SizedBox(width: 5,),
                              Text("${doctorList[index].star}", style: TextStyle(color: Colors.white),)
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: 10,),
                            Container(
                              height: 70,
                              width: 70,
                              //padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: Colors.amber,
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("${doctorList[index].avatar}"),
                                  fit: BoxFit.cover,
                                ),
                              ),

                            ),
                            SizedBox(height: 20,),
                            Text("${doctorList[index].name}", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                            SizedBox(height: 5.0,),
                            Container(
                              height: 30,
                              //width: 100,
                              margin: EdgeInsets.all(10.0),
                              //padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.blue[100],
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Center(
                                child: Text("${doctorList[index].type}", style: TextStyle(fontSize: 14, color: Colors.blue)),
                              ),
                            ),

                          ],
                        )
                      ],
                    ),
                  )
              ),
            );
          },
        )
    );
  }


}


