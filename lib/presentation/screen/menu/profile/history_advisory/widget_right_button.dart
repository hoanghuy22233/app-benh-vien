import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:flutter/material.dart';

class WidgetRightButton extends StatelessWidget {
  final Function onTap;
  final String text;
  final isEnable;

  const WidgetRightButton({Key key, this.onTap, this.text, this.isEnable})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 130,
      height: 30,
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          elevation: 2,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: isEnable
              ? Colors.blue[100]
              : AppColor.BUTTON_DISABLE_COLOR,
          child: Center(
              child: Text(
            text,
            style: isEnable
                ? AppStyle.DEFAULT_MEDIUM.copyWith(color: Colors.blue)
                : AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.BLACK),
          )),
        ),
      ),
    );
  }
}
