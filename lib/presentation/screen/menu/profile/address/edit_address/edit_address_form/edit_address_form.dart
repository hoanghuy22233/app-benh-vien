import 'dart:io';
import 'package:address_picker/address_picker.dart';
import 'package:base_code_project/app/constants/string/validator.dart';
import 'package:base_code_project/model/enitity_offline/address.dart';
import 'package:base_code_project/presentation/common_widgets/widget_delete_button.dart';
import 'package:base_code_project/presentation/common_widgets/widget_login_button.dart';
import 'package:base_code_project/presentation/common_widgets/widget_text_input.dart';
import 'package:base_code_project/presentation/screen/menu/profile/family_profile/item_dropdown.dart';
import 'package:flutter/material.dart';


class WidgetEditPersonForm extends StatefulWidget {
  final int id;

  const WidgetEditPersonForm({Key key, this.id}) : super(key: key);

  @override
  _WidgetEditPersonFormState createState() => _WidgetEditPersonFormState();
}

class _WidgetEditPersonFormState extends State<WidgetEditPersonForm> {
  TextEditingController _fullnameTextController = TextEditingController();
  TextEditingController _phoneNumberTextController = TextEditingController();
  TextEditingController _homeAddressTextController = TextEditingController();
  TextEditingController _cityTextController = TextEditingController();
  TextEditingController _districtTextController = TextEditingController();
  TextEditingController _wardTextController = TextEditingController();

  bool checkedValue = false;

  Item relations;
  Item genders;

  @override
  void initState() {
    super.initState();
    // _nameTextController.addListener(_onNameChange);
    // _descriptionTextController.addListener(_onDescriptionChange);
    _fullnameTextController.text = address[widget.id].name;
    _phoneNumberTextController.text = address[widget.id].phoneNumber;
    _homeAddressTextController.text = address[widget.id].homeAddress;
    _cityTextController.text = address[widget.id].city;
    _districtTextController.text = address[widget.id].district;
    _wardTextController.text = address[widget.id].ward;
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
        width: MediaQuery.of(context).size.width,
        //height: MediaQuery.of(context).size.height * 0.7,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Họ tên *"),
              _buildTextFieldFullName(),
              Divider(
                height: 0,
                thickness: 1,
                color: Colors.black12,
              ),
              SizedBox(height: 15,),
              Text("Số điện thoại *"),
              _buildTextFieldPhoneNumber(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              SizedBox(height: 15,),
              Text("Tỉnh/Thành phố *"),
              _buildTextFieldCity(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              SizedBox(height: 15,),
              Text("Quận/Huyện *"),
              _buildTextFieldPhoneDistrict(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              SizedBox(height: 15,),
              Text("Phường/Xã *"),
              _buildTextFieldWard(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              SizedBox(height: 15,),
              Text("Địa chỉ nhà *"),
              _buildTextFieldHomeAddress(),
              CheckboxListTile(
                title: Text("Đặt địa chỉ mặc định", style: TextStyle(fontSize: 14),),
                checkColor: Colors.white,
                activeColor: Colors.blue,
                value: checkedValue,
                onChanged: (newValue) {
                  setState(() {
                    checkedValue = !checkedValue;
                  });
                },
                controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
              ),
              SizedBox(height: 30,),

              _buildButtonDelete(),
              _buildButtonUpdate(),
            ],
          ),
        ),
      );
  }

  @override
  void dispose() {
    _fullnameTextController.dispose();
    _phoneNumberTextController.dispose();
    _homeAddressTextController.dispose();
    super.dispose();
  }

  _buildTextFieldFullName() {
    return WidgetTextInput(
      //autovalidate: autoValidate,
      inputController: _fullnameTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền họ tên"),
      hint: "Họ tên *",
    );
  }

  _buildTextFieldPhoneNumber() {
    return WidgetTextInput(
      inputType: TextInputType.number,
      //autovalidate: autoValidate,
      inputController: _phoneNumberTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền số điện thoại"),
      hint: "Số điện thoại *",
    );
  }

  _buildTextFieldHomeAddress() {
    return WidgetTextInput(
      //autovalidate: autoValidate,
      inputController: _homeAddressTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền địa chỉ nhà"),
      hint: "Địa chỉ nhà *",
    );
  }

  _buildTextFieldCity() {
    return WidgetTextInput(
      inputType: TextInputType.name,
      //autovalidate: autoValidate,
      inputController: _cityTextController,
      onChanged: (value) {
      },
      hint: "Tỉnh/Thành phố *",
    );
  }

  _buildTextFieldPhoneDistrict() {
    return WidgetTextInput(
      inputType: TextInputType.name,
      //autovalidate: autoValidate,
      inputController: _districtTextController,
      onChanged: (value) {
      },
      hint: "Quận/Huyện *",
    );
  }

  _buildTextFieldWard() {
    return WidgetTextInput(
      //autovalidate: autoValidate,
      inputController: _wardTextController,
      onChanged: (value) {
      },
      hint: "Phường/Xã *",
    );
  }

  _buildButtonUpdate(){
    return WidgetLoginButton(
      onTap: (){

      },
      text: "Cập nhật địa chỉ",
      isEnable: true
    );
  }

  _buildButtonDelete(){
    return WidgetDeleteButton(
        onTap: (){

        },
        backgroundColor: Colors.red[300],
        text: "Xóa",
        isEnable: true
    );
  }
}


List<Item> relationsList = <Item>[
  const Item('Bản thân',ImageIcon(AssetImage("assets/images/user.png.png"), color: Colors.red,)),
  const Item('Bố',ImageIcon(AssetImage("assets/images/dad.png"), color: Colors.red,)),
  const Item('Mẹ',ImageIcon(AssetImage("assets/images/mother.png"), color: Colors.red,)),
  const Item('Vợ',ImageIcon(AssetImage("assets/images/bride-bust.png"), color: Colors.red,)),
  const Item('Chồng',ImageIcon(AssetImage("assets/images/groom.png"), color: Colors.red,)),
];

List<Item> gendersList = <Item>[
  const Item('Nam', ImageIcon(AssetImage("assets/images/male-avatar.png"), color: Colors.blue,)),
  const Item('Nữ', ImageIcon(AssetImage("assets/images/woman-avatar.png"), color: Colors.pink,)),
  const Item('Khác',ImageIcon(AssetImage("assets/images/female-and-male.png"), color: Colors.green,)),
];

