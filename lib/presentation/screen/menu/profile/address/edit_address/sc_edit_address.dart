import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/menu/profile/address/edit_address/edit_address_form/edit_address_form.dart';
import 'package:base_code_project/presentation/screen/menu/profile/family_profile/add_person/widget_add_person_appbar.dart';
import 'package:flutter/material.dart';

class EditAddressScreen extends StatefulWidget {
  final int id;

  const EditAddressScreen({Key key, this.id}) : super(key: key);

  @override
  _EditAddressScreenState createState() => _EditAddressScreenState();
}

class _EditAddressScreenState extends State<EditAddressScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _appbar(),
          Expanded(
              child: ListView(
                padding: EdgeInsets.all(10),
                scrollDirection: Axis.vertical,
                children: [
                  _buildFormAddAddress(),
                ],
              )
          ),

        ],
      ),
    );
  }

  Widget _appbar() => WidgetAddPersonAppbar(titleText: "Cập nhật địa chỉ");

  Widget _buildFormAddAddress() => WidgetEditPersonForm(id: widget.id);

}
