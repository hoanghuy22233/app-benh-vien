import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/menu/profile/family_profile/add_person/widget_add_person_appbar.dart';
import 'package:flutter/material.dart';

import 'add_address_form/add_person_form.dart';

class AddAddressScreen extends StatefulWidget {
  @override
  _AddAddressScreenState createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _appbar(),
          Expanded(
              child: ListView(
                padding: EdgeInsets.all(0),
                scrollDirection: Axis.vertical,
                children: [
                  _buildFormAddAddress(),
                ],
              )
          ),

        ],
      ),
    );
  }

  Widget _appbar() => WidgetAddPersonAppbar(titleText: "Thêm địa chỉ");

  Widget _buildFormAddAddress() => WidgetAddPersonForm();

}
