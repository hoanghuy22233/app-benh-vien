import 'dart:io';
import 'package:address_picker/address_picker.dart';
import 'package:base_code_project/app/constants/string/validator.dart';
import 'package:base_code_project/presentation/common_widgets/widget_login_button.dart';
import 'package:base_code_project/presentation/common_widgets/widget_text_input.dart';
import 'package:base_code_project/presentation/screen/menu/profile/family_profile/item_dropdown.dart';
import 'package:flutter/material.dart';


class WidgetAddPersonForm extends StatefulWidget {
  @override
  _WidgetAddPersonFormState createState() => _WidgetAddPersonFormState();
}

class _WidgetAddPersonFormState extends State<WidgetAddPersonForm> {

  TextEditingController _fullnameTextController = TextEditingController();
  TextEditingController _phoneNumberTextController = TextEditingController();
  TextEditingController _homeAddressTextController = TextEditingController();

  bool checkedValue = false;

  Item relations;
  Item genders;

  @override
  void initState() {
    super.initState();
    // _nameTextController.addListener(_onNameChange);
    // _descriptionTextController.addListener(_onDescriptionChange);
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
        width: MediaQuery.of(context).size.width,
        //height: MediaQuery.of(context).size.height * 0.7,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Form(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Họ tên *"),
              _buildTextFieldFullName(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              Text("Số điện thoại *"),
              _buildTextFieldPhoneNumber(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              Text("Địa chỉ nhà *"),
              _buildTextFieldHomeAddress(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              _buildChooseLocation(),
              CheckboxListTile(
                title: Text("Đặt địa chỉ mặc định", style: TextStyle(fontSize: 14),),
                checkColor: Colors.white,
                activeColor: Colors.blue,
                value: checkedValue,
                onChanged: (newValue) {
                  setState(() {
                    checkedValue = !checkedValue;
                  });
                },
                controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
              ),
              SizedBox(height: 30,),
              _buildButtonCreate(),
            ],
          ),
        ),
      );
  }

  @override
  void dispose() {
    _fullnameTextController.dispose();
    _phoneNumberTextController.dispose();
    _homeAddressTextController.dispose();
    super.dispose();
  }

  _buildTextFieldFullName() {
    return WidgetTextInput(
      inputType: TextInputType.emailAddress,
      //autovalidate: autoValidate,
      inputController: _fullnameTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền họ tên"),
      hint: "Họ tên *",
    );
  }

  _buildTextFieldPhoneNumber() {
    return WidgetTextInput(
      inputType: TextInputType.emailAddress,
      //autovalidate: autoValidate,
      inputController: _phoneNumberTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền số điện thoại"),
      hint: "Số điện thoại *",
    );
  }

  _buildTextFieldHomeAddress() {
    return WidgetTextInput(
      //autovalidate: autoValidate,
      inputController: _homeAddressTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền địa chỉ nhà"),
      hint: "Địa chỉ nhà *",
    );
  }

  _buildButtonCreate(){
    return WidgetLoginButton(
      onTap: (){

      },
      text: "Thêm",
      isEnable: true
    );
  }
}

  Widget _buildChooseLocation() => AddressPicker(
    onAddressChanged: (address) {
      print(address);
    },
    underline: Container(color: Colors.transparent),
    placeHolderTextStyle: TextStyle(color: Colors.black, fontSize: 14),
    buildItem: (text) {
      return Text(text, style: TextStyle(color: Colors.black, fontSize: 14));
    },
  );

List<Item> relationsList = <Item>[
  const Item('Bản thân',ImageIcon(AssetImage("assets/images/user.png.png"), color: Colors.red,)),
  const Item('Bố',ImageIcon(AssetImage("assets/images/dad.png"), color: Colors.red,)),
  const Item('Mẹ',ImageIcon(AssetImage("assets/images/mother.png"), color: Colors.red,)),
  const Item('Vợ',ImageIcon(AssetImage("assets/images/bride-bust.png"), color: Colors.red,)),
  const Item('Chồng',ImageIcon(AssetImage("assets/images/groom.png"), color: Colors.red,)),
];

List<Item> gendersList = <Item>[
  const Item('Nam', ImageIcon(AssetImage("assets/images/male-avatar.png"), color: Colors.blue,)),
  const Item('Nữ', ImageIcon(AssetImage("assets/images/woman-avatar.png"), color: Colors.pink,)),
  const Item('Khác',ImageIcon(AssetImage("assets/images/female-and-male.png"), color: Colors.green,)),
];

