import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/model/enitity_offline/address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'edit_address/sc_edit_address.dart';

class ListAddress extends StatelessWidget {
  final int id;
  ListAddress({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => EditAddressScreen(id: id)
        ),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(address[id].name, style: TextStyle(fontSize: 14),),
                      Text(address[id].phoneNumber, style: TextStyle(fontSize: 14),),
                      Text(address[id].city, style: TextStyle(fontSize: 14),),
                    ],
                  )
              ),
              Expanded(
                  flex:1,
                  child: address[id].isDefault ?
                  Text("[Mặc định]", style: TextStyle(color: Colors.blue, fontSize: 14), textAlign: TextAlign.right,) :
                  Text("Đặt địa chỉ mặc định", style: TextStyle(color: Colors.cyan, fontSize: 14), textAlign: TextAlign.right,),
              ),
            ],
          ),
          Divider(
            height: 20,
            thickness: 1,
          ),
        ],
      ),
    );
  }
}
