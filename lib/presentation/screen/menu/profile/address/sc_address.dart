import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/model/enitity_offline/address.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/chat_with_doctor/chat_with_doctor.dart';
import 'package:base_code_project/presentation/screen/history_question/widget_history_question_appbar.dart';
import 'package:base_code_project/model/enitity_offline/listDoctorAdvisory.dart';
import 'package:base_code_project/presentation/screen/menu/profile/address/list_address.dart';
import 'package:base_code_project/presentation/screen/menu/profile/history_advisory/widget_right_button.dart';
import 'package:flutter/material.dart';


class AddressScreen extends StatefulWidget {
  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            _appbar(),
            Flexible(
               child: ListView.builder(
                 padding: EdgeInsets.zero,
                 scrollDirection: Axis.vertical,
                 itemCount: address.length +1,
                 itemBuilder: (context, index) {
                   if(index == address.length) {
                     return GestureDetector(
                       onTap: (){
                         AppNavigator.navigateAddNewAddressScreen();
                       },
                       child: Container(
                         height: 50,
                         margin: EdgeInsets.all(5),
                         decoration: BoxDecoration(
                             color: Colors.grey[300],
                             shape: BoxShape.rectangle
                         ),
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: [
                             Expanded(
                               flex:5,
                               child: Padding(
                                 padding: EdgeInsets.only(left: 10),
                                 child: Text("Thêm địa chỉ", style: TextStyle(color: Colors.black, fontSize: 14), textAlign: TextAlign.left,),
                               ),
                             ),
                             Expanded(
                               flex:1,
                               child: Icon(Icons.add, color: Colors.black),
                             ),
                           ],
                         ),
                       ),
                     );
                   }
                   return Container(
                     margin: EdgeInsets.all(10),
                     child: ListAddress(
                       id: index,
                     ),
                   );
                 },
               ),

            ),

          ],
        ),
    );
  }

  Widget _appbar() => WidgetDetailAppbar(titleText: "Danh sách địa chỉ");

  Widget _detailsAddress(String name, String phoneNumber, String city, bool isDefault){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: (){

              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(name, style: TextStyle(fontSize: 14),),
                  Text(phoneNumber, style: TextStyle(fontSize: 14),),
                  Text(city, style: TextStyle(fontSize: 14),),
                ],
              ),
            )
        ),
        Expanded(
            flex:1,
            child: GestureDetector(
              onTap: (){

              },
              child: isDefault ?
              Text("[Mặc định]", style: TextStyle(color: Colors.blue, fontSize: 14), textAlign: TextAlign.right,) :
              Text("Đặt địa chỉ mặc định", style: TextStyle(color: Colors.cyan, fontSize: 14), textAlign: TextAlign.right,),
            )
        ),
      ],
    );
  }

}


