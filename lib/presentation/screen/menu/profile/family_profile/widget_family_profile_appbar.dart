
import 'package:base_code_project/presentation/common_widgets/widget_appbar.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:flutter/material.dart';


class WidgetDetailAppbar extends StatelessWidget {
  final String titleText;

  const WidgetDetailAppbar({Key key, this.titleText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 20),
      child: WidgetAppbar(
        left: [WidgetAppbarMenuBack()],
        title: titleText,
      ),
    );
  }
}
