import 'package:base_code_project/presentation/screen/menu/profile/family_profile/add_person/widget_add_person_appbar.dart';
import 'package:base_code_project/presentation/screen/menu/profile/family_profile/detail_person/widget_detail_person_appbar.dart';
import 'package:flutter/material.dart';

import 'detail_person_form/detail_person_form.dart';

class DetailPersonScreen extends StatefulWidget {
  @override
  _DetailPersonScreenState createState() => _DetailPersonScreenState();
}

class _DetailPersonScreenState extends State<DetailPersonScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _appbar(),
          Expanded(
              child: ListView(
                padding: EdgeInsets.all(0),
                scrollDirection: Axis.vertical,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 3,
                          child: Container(
                            height: 50,
                            width: 50,
                            margin: EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle
                            ),
                            child: Image.asset("assets/images/user.png"),
                          ),
                        ),
                        Expanded(
                          flex: 6,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Ảnh đại diện", style: TextStyle(fontSize: 16),),
                            ],
                          ),
                        ),
                        Expanded(
                            flex:3,
                            child: Container(
                              height: 20,
                              width: 20,
                              margin: EdgeInsets.all(10.0),
                              alignment: Alignment.centerRight,
                              child: Text("Thay đổi", style: TextStyle(fontSize: 16, color: Colors.blue),),
                            )
                        ),
                      ],
                    ),
                  ),
                  Container(height: 10,),
                  _buildFormAddPerson(),
                ],
              )
          ),

        ],
      ),
    );
  }

  Widget _appbar() => WidgetDetailPersonAppbar(titleText: "Chi tiết người dùng dịch vụ");

Widget _buildFormAddPerson() => WidgetDetailPersonForm();

}
