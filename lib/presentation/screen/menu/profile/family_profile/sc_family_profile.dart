import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/model/enitity_offline/user.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/history_question/widget_history_question_appbar.dart';
import 'package:flutter/material.dart';

import 'detail_person/sc_detail_person.dart';
import 'edit_person/sc_edit_person.dart';
import 'item_dropdown.dart';


class FamilyProfileScreen extends StatefulWidget {
  @override
  _FamilyProfileScreenState createState() => _FamilyProfileScreenState();
}

class _FamilyProfileScreenState extends State<FamilyProfileScreen> {

  Item selectedUser;
  List<Item> items = <Item>[
    const Item('Sửa',ImageIcon(AssetImage("assets/images/draw.png"), color: Colors.green,)),
    const Item('Xóa',ImageIcon(AssetImage("assets/images/trash.png"), color: Colors.red,)),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            _appbar(),
            Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  scrollDirection: Axis.vertical,
                  itemCount: user.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => DetailPersonScreen()),
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Container(
                              height: 50,
                              width: 50,
                              margin: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle
                              ),
                              child: Image.asset(user[index].avatar),
                            ),
                          ),
                          Expanded(
                            flex: 6,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(user[index].name, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                                Text("${gendersList[user[index].gender].name}, chưa rõ tuổi", style: TextStyle(fontSize: 14),),
                              ],
                            ),
                          ),
                          Expanded(
                              flex:3,
                              child: Container(
                                height: 20,
                                width: 20,

                                //alignment: Alignment.centerRight,
                                child: DropdownButton<Item>(
                                  underline: Container(color: Colors.transparent),
                                  icon: Image.asset("assets/images/three-dots-more-indicator.png"),
                                  value: selectedUser,
                                  onChanged: (Item Value) {
                                    setState(() {
                                      if(Value.name == "Sửa"){
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) => EditPersonScreen()),
                                        );
                                      }
                                    });
                                  },
                                  items: items.map((Item item) {
                                    return  DropdownMenuItem<Item>(
                                      value: item,
                                      child: Row(
                                        children: <Widget>[
                                          item.icon,
                                          SizedBox(width: 10,),
                                          Text(
                                            item.name,
                                            style:  TextStyle(color: Colors.black),
                                          ),
                                        ],
                                      ),
                                    );
                                  }).toList(),
                                ),
                              )
                          ),
                        ],
                      ),
                    );
                  },
                )
            ),

          ],
        ),
      floatingActionButton: _buttonAdd(),
    );
  }

  Widget _appbar() => WidgetDetailAppbar(titleText: "Hồ sơ gia đình");

  Widget _buttonAdd() => FloatingActionButton(
    backgroundColor: Colors.blue,
    onPressed: (){
      AppNavigator.navigateAddPerson();
    },
    child: Icon(Icons.add, color: Colors.white,),
  );

  List<Item> gendersList = <Item>[
    const Item('Nam', ImageIcon(AssetImage("assets/images/male-avatar.png"), color: Colors.blue,)),
    const Item('Nữ', ImageIcon(AssetImage("assets/images/woman-avatar.png"), color: Colors.pink,)),
    const Item('Khác',ImageIcon(AssetImage("assets/images/female-and-male.png"), color: Colors.green,)),
  ];
}
