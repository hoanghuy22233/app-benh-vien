import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/menu/profile/family_profile/add_person/widget_add_person_appbar.dart';
import 'package:flutter/material.dart';

import 'add_person_form/add_person_form.dart';



class AddPersonScreen extends StatefulWidget {
  @override
  _AddPersonScreenState createState() => _AddPersonScreenState();
}

class _AddPersonScreenState extends State<AddPersonScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _appbar(),
          Expanded(
              child: ListView(
                padding: EdgeInsets.all(0),
                scrollDirection: Axis.vertical,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 3,
                          child: Container(
                            height: 50,
                            width: 50,
                            margin: EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle
                            ),
                            child: Image.asset("assets/images/user.png"),
                          ),
                        ),
                        Expanded(
                          flex: 6,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Ảnh đại diện", style: TextStyle(fontSize: 16),),
                            ],
                          ),
                        ),
                        Expanded(
                            flex:3,
                            child: Container(
                              height: 20,
                              width: 20,
                              margin: EdgeInsets.all(10.0),
                              alignment: Alignment.centerRight,
                              child: Text("Thay đổi", style: TextStyle(fontSize: 16, color: Colors.blue),),
                            )
                        ),
                      ],
                    ),
                  ),
                  Container(height: 10,),
                  _buildFormAddPerson(),
                ],
              )
          ),

        ],
      ),
    );
  }

  Widget _appbar() => WidgetAddPersonAppbar(titleText: "Tạo người dùng dịch vụ");

Widget _buildFormAddPerson() => WidgetAddPersonForm();

}
