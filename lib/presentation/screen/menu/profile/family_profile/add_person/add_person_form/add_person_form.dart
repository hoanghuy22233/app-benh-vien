import 'dart:io';
import 'package:base_code_project/app/constants/string/validator.dart';
import 'package:base_code_project/presentation/common_widgets/widget_login_button.dart';
import 'package:base_code_project/presentation/common_widgets/widget_text_input.dart';
import 'package:base_code_project/presentation/screen/menu/profile/family_profile/item_dropdown.dart';
import 'package:flutter/material.dart';


class WidgetAddPersonForm extends StatefulWidget {
  @override
  _WidgetAddPersonFormState createState() => _WidgetAddPersonFormState();
}

class _WidgetAddPersonFormState extends State<WidgetAddPersonForm> {

  TextEditingController _fullnameTextController = TextEditingController();
  TextEditingController _phoneNumberTextController = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _dobTextController = TextEditingController();

  bool checkedValue = false;

  Item relations;
  Item genders;

  @override
  void initState() {
    super.initState();
    // _nameTextController.addListener(_onNameChange);
    // _descriptionTextController.addListener(_onDescriptionChange);
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
        width: MediaQuery.of(context).size.width,
        //height: MediaQuery.of(context).size.height * 0.7,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Form(
          child: Column(
            children: [
              _buildTextFieldFullName(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.only(left: 10),
                      child: DropdownButton<Item>(
                        hint:  Text("Mối quan hệ"),
                        value: relations,
                        onChanged: (Item _value) {
                          setState(() {
                            relations = _value;
                          });
                        },
                        items: relationsList.map((Item user) {
                          return  DropdownMenuItem<Item>(
                            value: user,
                            child: Row(
                              children: <Widget>[
                                user.icon,
                                SizedBox(width: 10,),
                                Text(
                                  user.name,
                                  style:  TextStyle(color: Colors.black),
                                ),
                              ],
                            ),
                          );
                        }).toList(),
                      ),
                    )
                  ),
                  SizedBox(width: 20,),
                  Expanded(
                    flex: 1,
                    child: Container(
                      child: DropdownButton<Item>(
                        hint:  Text("Giới tính"),
                        value: genders,
                        onChanged: (Item _value) {
                          setState(() {
                            genders = _value;
                          });
                        },
                        items: gendersList.map((Item user) {
                          return  DropdownMenuItem<Item>(
                            value: user,
                            child: Row(
                              children: <Widget>[
                                user.icon,
                                SizedBox(width: 10,),
                                Text(
                                  user.name,
                                  style:  TextStyle(color: Colors.black),
                                ),
                              ],
                            ),
                          );
                        }).toList(),
                      ),
                    )
                  ),
                ],
              ),
              _buildTextFieldDOB(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              _buildTextFieldPhoneNumber(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),
              _buildTextFieldEmail(),
              Divider(
                height: 5,
                thickness: 1,
                color: Colors.black12,
              ),

              CheckboxListTile(
                title: Text("Chọn làm người dùng mặc định", style: TextStyle(fontSize: 14),),
                checkColor: Colors.white,
                activeColor: Colors.blue,
                value: checkedValue,
                onChanged: (newValue) {
                  setState(() {
                    checkedValue = !checkedValue;
                  });
                },
                controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
              ),
              SizedBox(height: 30,),
              _buildButtonCreate(),
            ],
          ),
        ),
      );
  }

  @override
  void dispose() {
    _fullnameTextController.dispose();
    _phoneNumberTextController.dispose();
    _emailTextController.dispose();
    _dobTextController.dispose();
    super.dispose();
  }

  _buildTextFieldFullName() {
    return WidgetTextInput(
      inputType: TextInputType.emailAddress,
      //autovalidate: autoValidate,
      inputController: _fullnameTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền họ tên"),
      hint: "Họ tên *",
    );
  }

  _buildTextFieldPhoneNumber() {
    return WidgetTextInput(
      inputType: TextInputType.emailAddress,
      //autovalidate: autoValidate,
      inputController: _phoneNumberTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền số điện thoại"),
      hint: "Số điện thoại *",
    );
  }

  _buildTextFieldEmail() {
    return WidgetTextInput(
      inputType: TextInputType.emailAddress,
      //autovalidate: autoValidate,
      inputController: _emailTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền email"),
      hint: "Email *",
    );
  }

  _buildTextFieldDOB() {
    return WidgetTextInput(
      inputType: TextInputType.emailAddress,
      //autovalidate: autoValidate,
      inputController: _dobTextController,
      onChanged: (value) {
      },
      validator: AppValidation.validateUserName("Vui lòng điền ngày sinh"),
      hint: "Ngày sinh *",
    );
  }

  _buildButtonCreate(){
    return WidgetLoginButton(
      onTap: (){

      },
      text: "Tạo người dùng",
      isEnable: true
    );
  }
}

List<Item> relationsList = <Item>[
  const Item('Bản thân',ImageIcon(AssetImage("assets/images/user.png.png"), color: Colors.red,)),
  const Item('Bố',ImageIcon(AssetImage("assets/images/dad.png"), color: Colors.red,)),
  const Item('Mẹ',ImageIcon(AssetImage("assets/images/mother.png"), color: Colors.red,)),
  const Item('Vợ',ImageIcon(AssetImage("assets/images/bride-bust.png"), color: Colors.red,)),
  const Item('Chồng',ImageIcon(AssetImage("assets/images/groom.png"), color: Colors.red,)),
];

List<Item> gendersList = <Item>[
  const Item('Nam', ImageIcon(AssetImage("assets/images/male-avatar.png"), color: Colors.blue,)),
  const Item('Nữ', ImageIcon(AssetImage("assets/images/woman-avatar.png"), color: Colors.pink,)),
  const Item('Khác',ImageIcon(AssetImage("assets/images/female-and-male.png"), color: Colors.green,)),
];

