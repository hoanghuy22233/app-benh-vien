import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Item {
  const Item(this.name,this.icon);

  final String name;
  final ImageIcon icon;

}