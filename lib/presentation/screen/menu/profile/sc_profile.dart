import 'package:base_code_project/app/auth_bloc/authentication_bloc.dart';
import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/repo/barrel_repo.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/menu/profile/widget_profile_appbar.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool selected1=false;
  bool selected2=false;
  bool selected3=false;
  double percent=0;
  double percentT=0;
  String percentText = "0";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            _appbar(),
            Expanded(
                child: ListView(
                  padding: EdgeInsets.all(10),
                  scrollDirection: Axis.vertical,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                            height: 50,
                            width: 50,
                            margin: EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle
                            ),
                            child: Image.asset("assets/images/user.png"),
                          ),
                        ),
                        Expanded(
                          flex: 6,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Chưa cập nhật tên", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                              Text("Thông tin tài khoản, cài đặt", style: TextStyle(fontSize: 14),),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Container(
                              alignment: Alignment.centerRight,
                              child: Icon(Icons.keyboard_arrow_right),
                            )
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Card(
                        color: Colors.blue[200],
                        child: Container(
                          margin: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Hoàn thiện hồ sơ cá nhân", style: TextStyle(fontSize: 16, color: Colors.white)),
                              SizedBox(height: 10,),
                              LinearPercentIndicator(
                                width: MediaQuery.of(context).size.width - 50,
                                animation: true,
                                lineHeight: 18.0,
                                animationDuration: 300,
                                percent: percent,
                                center: Text("$percentText %", style: TextStyle(color: Colors.blue)),
                                linearStrokeCap: LinearStrokeCap.roundAll,
                                progressColor: Colors.white,
                              ),
                              SizedBox(height: 10,),
                              Text("Hoàn thiện thông tin email và số điện thoại của bạn để ứng dụng cung cấp dịch vụ tốt nhất cho sức khỏe của bạn nhé!", style: TextStyle(fontSize: 14, color: Colors.white), textAlign: TextAlign.justify, ),
                              Divider(
                                thickness: 1,
                                height: 20,
                              ),
                              Container(
                                height: 25,
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: CircularCheckBox(
                                          value: this.selected1,
                                          checkColor: Colors.blue[200],
                                          activeColor: Colors.white,
                                          inactiveColor: Colors.white,
                                          disabledColor: Colors.grey,
                                          materialTapTargetSize: MaterialTapTargetSize.padded,
                                          onChanged: (val) => this.setState(() {
                                            this.selected1= !this.selected1 ;
                                            if(selected1 == true) {
                                              percent = percent + 0.25;
                                              percentT = percent*100;
                                              percentText = percentT.toString();
                                            }
                                            else {
                                              percent = percent - 0.25;
                                              percentT = percent*100;
                                              percentText = percentT.toString();
                                            }
                                          })
                                      ),
                                    ),
                                    Expanded(
                                      flex: 8,
                                      child: Text("Liên kết số điện thoại", style: TextStyle(fontSize: 14, color: Colors.white),),
                                    ),
                                  ],
                                ),
                              ),
                              Divider(
                                thickness: 1,
                                height: 20,
                              ),
                              Container(
                                height: 25,
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: CircularCheckBox(
                                          value: this.selected2,
                                          checkColor: Colors.blue[200],
                                          activeColor: Colors.white,
                                          inactiveColor: Colors.white,
                                          disabledColor: Colors.grey,
                                          materialTapTargetSize: MaterialTapTargetSize.padded,
                                          onChanged: (val) => this.setState(() {
                                            this.selected2= !this.selected2 ;
                                            if(selected2 == true) {
                                              percent = percent + 0.25;
                                              percentT = percent*100;
                                              percentText = percentT.toString();
                                            }
                                            else {
                                              percent = percent - 0.25;
                                              percentT = percent*100;
                                              percentText = percentT.toString();
                                            }
                                          })
                                      ),
                                    ),
                                    Expanded(
                                      flex: 8,
                                      child: Text("Liên kết email để nhận thông báo", style: TextStyle(fontSize: 14, color: Colors.white),),
                                    ),
                                  ],
                                ),
                              ),

                              Divider(
                                thickness: 1,
                                height: 20,
                              ),
                              Container(
                                height: 25,
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: CircularCheckBox(
                                          value: this.selected3,
                                          checkColor: Colors.blue[200],
                                          activeColor: Colors.white,
                                          inactiveColor: Colors.white,
                                          disabledColor: Colors.grey,
                                          materialTapTargetSize: MaterialTapTargetSize.padded,
                                          onChanged: (val) => this.setState(() {
                                            this.selected3 = !this.selected3 ;
                                            if(selected3 == true) {
                                              percent = percent + 0.5;
                                              percentT = percent*100;
                                              percentText = percentT.toString();
                                            }
                                            else {
                                              percent = percent - 0.5;
                                              percentT = percent*100;
                                              percentText = percentT.toString();
                                            }
                                          }
                                          )
                                      ),
                                    ),
                                    Expanded(
                                      flex: 8,
                                      child: Text("Hoàn thiện thông tin cá nhân", style: TextStyle(fontSize: 14, color: Colors.white),),
                                    ),
                                  ],
                                ),
                              ),

                            ],

                          ),
                        )
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height / 2,
                      child: GridView.count(
                        physics: NeverScrollableScrollPhysics(),
                        primary: false,
                        //padding: const EdgeInsets.all(5),
                        crossAxisSpacing: 5,
                        mainAxisSpacing: 5,
                        crossAxisCount: 2,
                        childAspectRatio: 1.8,

                        children: <Widget>[
                          GestureDetector(
                            onTap: (){
                              AppNavigator.navigateTestResult();
                            },
                            child: Card(
                                color: Colors.blue[50],
                                child: Container(
                                  margin: EdgeInsets.only(top: 15, left: 15, right:15),
                                  //padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 25,
                                        width: 25,
                                        padding: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            shape: BoxShape.circle
                                        ),
                                        child: Image.asset("assets/images/flask.png", color: Colors.white,),
                                      ),
                                      SizedBox(height: 10,),
                                      Text("Kết quả xét nghiệm", style: TextStyle(fontSize: 14))
                                    ],
                                  ),
                                )
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              AppNavigator.navigateHistoryAdvisory();
                            },
                            child: Card(
                                color: Colors.blue[50],
                                child: Container(
                                  margin: EdgeInsets.only(top: 15, left: 15, right:15),
                                  //padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 25,
                                        width: 25,
                                        padding: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            shape: BoxShape.circle
                                        ),
                                        child: Image.asset("assets/images/question.png", color: Colors.white,),
                                      ),
                                      SizedBox(height: 10,),
                                      Text("Lịch sử tư vấn", style: TextStyle(fontSize: 14))
                                    ],
                                  ),
                                )
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              AppNavigator.navigateFamilyProfile();
                            },
                            child: Card(
                                color: Colors.blue[50],
                                child: Container(
                                  margin: EdgeInsets.only(top: 15, left: 15, right:15),
                                  //padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 25,
                                        width: 25,
                                        padding: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            shape: BoxShape.circle
                                        ),
                                        child: Image.asset("assets/images/family-room.png", color: Colors.white,),
                                      ),
                                      SizedBox(height: 10,),
                                      Text("Hồ sơ gia đình", style: TextStyle(fontSize: 14))
                                    ],
                                  ),
                                )
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              AppNavigator.navigateAddressScreen();
                            },
                            child: Card(
                                color: Colors.blue[50],
                                child: Container(
                                  margin: EdgeInsets.only(top: 15, left: 15, right:15),
                                  //padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 25,
                                        width: 25,
                                        padding: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            shape: BoxShape.circle
                                        ),
                                        child: Image.asset("assets/images/notebook.png", color: Colors.white ),
                                      ),
                                      SizedBox(height: 10,),
                                      Text("Sổ địa chỉ", style: TextStyle(fontSize: 14))
                                    ],
                                  ),
                                )
                            ),
                          ),
                          GestureDetector(
                            onTap: (){

                            },
                            child: Card(
                                color: Colors.blue[50],
                                child: Container(
                                  margin: EdgeInsets.only(top: 15, left: 15, right:15),
                                  //padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 25,
                                        width: 25,
                                        padding: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            shape: BoxShape.circle
                                        ),
                                        child: Image.asset("assets/images/credit-card.png", color: Colors.white),
                                      ),
                                      SizedBox(height: 10,),
                                      Text("Thông tin thanh toán", style: TextStyle(fontSize: 14))
                                    ],
                                  ),
                                )
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              AppNavigator.navigateNewCalendarReminderScreen();
                            },
                            child: Card(
                                color: Colors.blue[50],
                                child: Container(
                                  margin: EdgeInsets.only(top: 15, left: 15, right:15),
                                  //padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 25,
                                        width: 25,
                                        padding: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            shape: BoxShape.circle
                                        ),
                                        child: Image.asset("assets/images/alarm-clock.png", color: Colors.white,),
                                      ),
                                      SizedBox(height: 10,),
                                      Text("Lời nhắc", style: TextStyle(fontSize: 14))
                                    ],
                                  ),
                                )
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )
            ),

          ],
        )
    );
  }

  Widget _appbar() => WidgetProfileAppbar();
}
