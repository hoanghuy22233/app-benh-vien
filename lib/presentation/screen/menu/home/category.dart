import 'package:base_code_project/model/enitity_offline/category.dart';
import 'package:base_code_project/utils/color/hex_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Category extends StatefulWidget {
  final int id;
  Category({Key key, this.id}) : super(key: key);

  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: null,
        child: Container(
          child: Column(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                      color: Color(HexColor.getColorFromHex(itemCategory[widget.id].colorImage)),
                      borderRadius: BorderRadius.circular(500),
                    ),
                    child: null,
                  ),
                  Image.asset('${itemCategory[widget.id].image}' , width: 25, height: 25, color: Colors.white,),
                ],
              ),
              SizedBox(height: 5,),
              Text(
                '${itemCategory[widget.id].name}',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 10  ,
                    fontWeight: FontWeight.bold
                ),
              ),
            ],
          )),
        );
  }
}
