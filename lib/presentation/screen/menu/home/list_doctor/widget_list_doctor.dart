import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/enitity_offline/askDoctor.dart';
import 'package:base_code_project/model/enitity_offline/category.dart';
import 'package:base_code_project/model/enitity_offline/list_doctor.dart';
import 'package:base_code_project/presentation/screen/menu/home/category_all.dart';
import 'package:base_code_project/presentation/screen/menu/home/list_doctor/widget_list_item_doctor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class WidgetListDoctor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      child: ListView.builder(
        padding: EdgeInsets.zero,
        scrollDirection: Axis.horizontal,
        itemCount: itemDoctor.length,
        itemBuilder: (context, index) {
          return Container(
            child: WidgetListItemDoctor(

              id: index,
            ),
          );
        },
      ),
    );
  }


}
