import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/model/enitity_offline/list_doctor.dart';
import 'package:base_code_project/presentation/screen/detail_doctor/detail_doctor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetListItemDoctor extends StatefulWidget {
  final int id;
  WidgetListItemDoctor({Key key, this.id}) : super(key: key);

  @override
  _WidgetListItemDoctorState createState() => _WidgetListItemDoctorState();
}

class _WidgetListItemDoctorState extends State<WidgetListItemDoctor> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => DetailDoctorScreen(id: widget.id)),

      ),

      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
          child: Column(
            children: [
              Card(
                elevation: 2,
                child: Image.asset(itemDoctor[widget.id].image,height: 150,width: 150,fit: BoxFit.cover,),
              ),
              SizedBox(height: 5,),
              Text(
                itemDoctor[widget.id].name.length <=
                    15
                    ? 'Bs.${itemDoctor[widget.id].name}'
                    : 'Bs.${itemDoctor[widget.id].name.substring(0, 15)+
                    '...'}',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16  ,
                    fontWeight: FontWeight.bold
                ),
              ),
            ],
          )),
        );
  }
}
