import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/model/enitity_offline/category.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryAll extends StatefulWidget {
  final int id;
  CategoryAll({Key key, this.id}) : super(key: key);

  @override
  _CategoryAllState createState() => _CategoryAllState();
}

class _CategoryAllState extends State<CategoryAll> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: null,
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            Card(
              elevation: 2,
              child: Container(
                height: 120,width: MediaQuery.of(context).size.width/2,
                child: Image.asset(itemCategory[widget.id].background,fit: BoxFit.cover,),
              ),
            ),
           Container(
             margin: EdgeInsets.only(right: 10,top: 10),
             child:  Text(
               itemCategory[widget.id].name.length <=
                   100
                   ? itemCategory[widget.id].name
                   : itemCategory[widget.id]
                   .name
                   .substring(0, 100) +
                   '...',
               style: AppStyle.DEFAULT_SMALL
                   .copyWith(color: AppColor.WHITE,fontWeight: FontWeight.bold),
               textAlign: TextAlign.start,
             ),
           )
          ],
        ),
        );
  }
}
