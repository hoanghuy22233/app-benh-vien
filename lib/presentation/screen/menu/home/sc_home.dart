import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/enitity_offline/askDoctor.dart';
import 'package:base_code_project/model/enitity_offline/category.dart';
import 'package:base_code_project/presentation/screen/menu/home/category_all.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'askAndAnswer_listView.dart';
import 'category.dart';
import 'list_doctor/widget_list_doctor.dart';

class HomeScreen extends StatefulWidget {
  GlobalKey<ScaffoldState> drawer;
  final Function action;
  HomeScreen({this.drawer, this.action});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[300],
      body: Stack(
        children: <Widget>[
          NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: MediaQuery.of(context).size.height / 2,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      background: _buildContent(),
                    ),
                  ),
                ];
              },
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                            )),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 5,
                            ),
                            Center(
                              child: Icon(
                                Icons.keyboard_arrow_up_rounded,
                                color: Colors.blue[300],
                                size: 30,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                'Hỏi Bác sĩ - Khoẻ hơn mỗi ngày',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Divider(
                              thickness: 0.5,
                              color: Colors.grey,
                            ),
                            Container(
                              height: 300,
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                padding: EdgeInsets.zero,
                                scrollDirection: Axis.vertical,
                                itemCount: askAndAsnwerItem.length +
                                    2 -
                                    askAndAsnwerItem.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    child: AskAndAnswerListVie(
                                      id: index,
                                    ),
                                  );
                                },
                              ),
                            ),
                            Card(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              child: GestureDetector(
                                onTap: () {
                                  widget.action(page: 2);
                                },
                                child: Container(
                                  color: Colors.blue[100],
                                  height: 45,
                                  width: MediaQuery.of(context).size.width,
                                  child: Center(
                                    child: Text(
                                      "Khám phá cộng đồng",
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Divider(
                              thickness: 0.5,
                              color: Colors.grey,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                children: [
                                  Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Container(
                                        height: 45,
                                        width: 45,
                                        decoration: BoxDecoration(
                                          color: Colors.blue[100],
                                          borderRadius:
                                              BorderRadius.circular(500),
                                        ),
                                      ),
                                      Image.asset(
                                        'assets/images/houses.png',
                                        width: 25,
                                        height: 25,
                                        color: Colors.white,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Bác sĩ dành cho bạn",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        "Giải đáp thắc mắc sức khỏe 24/7",
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 14),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            _buildDoctor()
                          ],
                        )),
                  ],
                ),
              )),
          _buildHomeContent(),
        ],
      ),
    );
  }

  Widget _buildHomeContent() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 25),
            color: Colors.blue,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      'assets/images/img_logo.png',
                      width: 40,
                      height: 40,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "e",
                            style:
                                TextStyle(color: Colors.white, fontSize: 24)),
                        TextSpan(
                            text: "VietTiep",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 24)),
                      ]),
                    ),
                  ],
                ),
                Spacer(),
                Image.asset(
                  'assets/icons/nav_user.png',
                  width: 40,
                  height: 40,
                  color: Colors.white,
                ),
                SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
          Container(
            color: Colors.blue,
            child: Row(
              children: [
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.location_city,
                  size: 40,
                  color: Colors.blue[100],
                ),
                SizedBox(
                  width: 5,
                ),
                Container(
                  width: 120,
                  height: 30,
                  decoration: BoxDecoration(
                    color: Colors.blue[100],
                    borderRadius: BorderRadius.circular(7),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Hải Phòng',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 10,
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Container(
      margin: EdgeInsets.only(top: 120),
      height: 400,
      child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
            child: GridView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.only(top: 10, left: 5, right: 5),
                itemBuilder: (context, index) {
                  if (index == itemCategory.length) {
                    return GestureDetector(
                      onTap: () async {
                        displayBottomSheet(context);
                      },
                      child: Container(
                          child: Column(
                        children: [
                          Stack(
                            alignment: Alignment.center,
                            children: [
                              Container(
                                height: 45,
                                width: 45,
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(500),
                                ),
                                child: null,
                              ),
                              Image.asset(
                                'assets/images/ic_more.png',
                                width: 25,
                                height: 25,
                                color: Colors.white,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            'Xem nhiều hơn',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 10,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )),
                    );
                  }
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: Category(
                      id: index,
                    ),
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 5,
                    childAspectRatio: 0.85),
                physics: NeverScrollableScrollPhysics(),
                itemCount: itemCategory.length + 1),
          )),
    );
  }

  void displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
      backgroundColor: Colors.white,
      context: context,
      isScrollControlled: true,
      builder: (context) => Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height - 100,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    "Tất cả dịch vụ",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Expanded(
                  child: GridView.builder(

                      shrinkWrap: true,
                      padding: EdgeInsets.only(top: 10, left: 5, right: 5),
                      itemBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.only(bottom: 20),
                          child: CategoryAll(
                            id: index,
                          ),
                        );
                      },
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 5,
                          childAspectRatio: 1.3),
                      // physics: NeverScrollableScrollPhysics(),
                      itemCount: itemCategory.length),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, top: 10),
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateBack();
              },
              child: Image.asset(
                "assets/images/img_close_round.png",
                height: 25,
                width: 25,
              ),
            ),
          )
        ],
      ),
    );
  }

  _buildDoctor() => WidgetListDoctor();
}
