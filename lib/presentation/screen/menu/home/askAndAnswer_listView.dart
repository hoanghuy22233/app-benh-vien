import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/model/enitity_offline/askDoctor.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:base_code_project/presentation/screen/detail_comment/sc_social_list_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AskAndAnswerListVie extends StatefulWidget {
  final int id;
  AskAndAnswerListVie({Key key, this.id}) : super(key: key);
  @override
  _AskAndAnswerListVieState createState() => _AskAndAnswerListVieState();
}

class _AskAndAnswerListVieState extends State<AskAndAnswerListVie> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: GestureDetector(
        child: Container(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Row(
                  children: [
                    Container(
                      height: 25,
                      decoration: BoxDecoration(
                          color: Colors.blue[100],
                          borderRadius: BorderRadius.circular(5)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SizedBox(
                            width: 5,
                          ),
                          Image.asset(
                            "${askAndAsnwerItem[widget.id].titleIcon}",
                            width: 20,
                            height: 20,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            '${askAndAsnwerItem[widget.id].title}',
                            style: TextStyle(
                                color: Colors.blue[300],
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '${askAndAsnwerItem[widget.id].sex}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          '${askAndAsnwerItem[widget.id].time}',
                          style: TextStyle(fontSize: 10),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Text(
                  '${askAndAsnwerItem[widget.id].question}',
                  style: TextStyle(fontWeight: FontWeight.bold),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 75,
                child: Stack(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 25),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(500),
                          child: Image.asset(
                            '${askAndAsnwerItem[widget.id].avatarDoctor}',
                            width: 50,
                            height: 50,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  askAndAsnwerItem[widget.id].nameDoctor,
                                  style: AppStyle.DEFAULT_SMALL.copyWith(
                                      color: AppColor.THIRD_COLOR,
                                      fontWeight: FontWeight.bold),
                                ),
                                WidgetSpacer(
                                  height: 3,
                                ),
                                Expanded(
                                  child: Container(
                                      child: Text(
                                    askAndAsnwerItem[widget.id].answer.length <=
                                            100
                                        ? askAndAsnwerItem[widget.id].answer
                                        : askAndAsnwerItem[widget.id]
                                                .answer
                                                .substring(0, 100) +
                                            '...',
                                    style: AppStyle.DEFAULT_SMALL
                                        .copyWith(color: AppColor.GREY),
                                    textAlign: TextAlign.start,
                                  )),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Stack(
                    children: [
                      Container(
                        height: 30,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                              flex: 7,
                              child: Container(
                                  height: 30,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.star,
                                          size: 20,
                                          color: Colors.grey[400],
                                        ),
                                        Text(
                                          'Lưu vào xem sau',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))),
                          Expanded(
                            flex: 4,
                            child: GestureDetector(
                              onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        NewsDetailNewsPage(id: widget.id)),
                              ),
                              child: Container(
                                  height: 30,
                                  decoration: BoxDecoration(
                                    color: Colors.blue[300],
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Chi Tiết",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        Icon(
                                          Icons.chevron_right_outlined,
                                          size: 15,
                                          color: Colors.white,
                                        ),
                                      ],
                                    ),
                                  )),
                            ),
                          )
                        ],
                      ),
                    ],
                  )),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
