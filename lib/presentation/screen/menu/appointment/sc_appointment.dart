import 'package:base_code_project/presentation/screen/menu/appointment/widget_appointment_appbar.dart';
import 'package:flutter/material.dart';


class AppointmentScreen extends StatefulWidget {
  @override
  _AppointmentScreenState createState() => _AppointmentScreenState();
}

class _AppointmentScreenState extends State<AppointmentScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            _appbar(),
            Expanded(
                child: ListView(
                  padding: EdgeInsets.all(10),
                  scrollDirection: Axis.vertical,
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.79,
                      decoration: BoxDecoration(
                          color: Colors.white
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Center(
                              child: Container(
                                height: 200,
                                width: 200,
                                child: Image.asset("assets/images/appointment_background.png"),
                              )
                          ),
                          Center(
                            child: Text("Bạn chưa có lịch hẹn!", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),),
                          ),
                          SizedBox(height: 10,),
                          Center(
                            child: Text("Đặt ngay một dịch vụ chăm sóc sức khỏe cho bạn và gia đình", style: TextStyle(fontSize: 14),textAlign: TextAlign.center,),
                          )
                        ],
                      ),
                    )
                  ],
                )
            ),

          ],
        )
    );
  }

  Widget _appbar() => WidgetAppointmentAppbar();
}
