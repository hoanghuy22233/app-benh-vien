import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/model/enitity_offline/askDoctor.dart';
import 'package:base_code_project/presentation/screen/detail_comment/sc_social_list_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetListItemSocial extends StatefulWidget {
  final int id;
  WidgetListItemSocial({Key key, this.id}) : super(key: key);

  @override
  _WidgetListItemSocialState createState() => _WidgetListItemSocialState();
}

class _WidgetListItemSocialState extends State<WidgetListItemSocial> {
  bool isReadDetail = false;
  String textCategory =
      "Chào bạn, cảm ơn bạn đã gửi câu hỏi. Bạn cần có chế độ ăn uống. Vận động và nghỉ ngơi hợp lý. bạn có thể gặp bác sĩ chuyên khoa cơ xương khớp để biết rõ hơn";
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: [
              Container(
                child: Text(
                  askAndAsnwerItem[widget.id].question,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 13,
                      fontWeight: FontWeight.bold),
                  maxLines: 4,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Row(
                  children: [
                    Expanded(
                        child: Text(
                      askAndAsnwerItem[widget.id].sex,
                      style: TextStyle(color: Colors.grey),
                    )),
                    Text(
                      askAndAsnwerItem[widget.id].time,
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 1,
                height: MediaQuery.of(context).size.height / 4,
                color: Colors.grey[100],
                margin: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: 20,
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(500),
                          child: Image.asset(
                            "assets/images/avatar_doctor.png",
                            height: 40,
                            // fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          // flex: 8,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      "BS. Lại Xuân Lợi",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "2 giờ trước",
                                style: TextStyle(fontSize: 14),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        textCategory.length <= 150
                            ? textCategory
                            : textCategory.substring(0, 150) + '...',
                        style:
                            AppStyle.DEFAULT_SMALL.copyWith(color: Colors.grey),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    GestureDetector(
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                NewsDetailNewsPage(id: widget.id)),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text('Xem câu trả lời',
                              style:
                                  TextStyle(fontSize: 12, color: Colors.blue)),
                          SizedBox(
                            width: 3,
                          ),
                          Icon(
                            Icons.arrow_forward_ios_outlined,
                            color: Colors.blue,
                            size: 12,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      width: 50,
                    ),
                    Text("${askAndAsnwerItem[widget.id].share} chia sẻ"),
                    SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      "assets/images/img_share_color.png",
                      height: 15,
                      width: 15,
                    ),
                    SizedBox(
                      width: 45,
                    ),
                    Text("${askAndAsnwerItem[widget.id].likeCount} lượt thích"),
                    SizedBox(
                      width: 10,
                    ),
                    askAndAsnwerItem[widget.id].like == 0
                        ? Image.asset(
                            "assets/icons/ic_heart_gray.png",
                            height: 15,
                            width: 15,
                          )
                        : Image.asset(
                            "assets/icons/ic_heart_red.png",
                            height: 15,
                            width: 15,
                          ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
