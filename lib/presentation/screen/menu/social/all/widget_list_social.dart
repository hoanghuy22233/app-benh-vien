import 'package:base_code_project/model/enitity_offline/askDoctor.dart';
import 'package:base_code_project/presentation/screen/menu/social/all/widget_list_item_social.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetListSocial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      child: ListView.builder(
        padding: EdgeInsets.zero,
        scrollDirection: Axis.vertical,
        itemCount: askAndAsnwerItem.length,
        itemBuilder: (context, index) {
          return Container(
            child: WidgetListItemSocial(
              id: index,
            ),
          );
        },
      ),
    );
  }
}
