import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/model/enitity_offline/category.dart';
import 'package:base_code_project/model/enitity_offline/categoryseach.dart';
import 'package:base_code_project/presentation/screen/menu/social/all/widget_list_social.dart';
import 'package:base_code_project/presentation/screen/menu/social/category_professional.dart';
import 'package:base_code_project/presentation/screen/menu/social/category_social.dart';
import 'package:base_code_project/presentation/screen/menu/social/widget_comment_question.dart';
import 'package:base_code_project/presentation/screen/menu/social/widget_social_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SocialScreen extends StatefulWidget {
  GlobalKey<ScaffoldState> drawer;

  SocialScreen({this.drawer});
  @override
  _SocialScreenState createState() => _SocialScreenState();
}

class _SocialScreenState extends State<SocialScreen> {
  String textNode = 'Tất cả danh mục chuyên môn';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            Column(
              children: <Widget>[
                _buildAppbar(),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          "Cộng đồng",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Card(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          height: 30,
                          color: Colors.grey[100],
                          child: Center(
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  child: Image.asset(
                                    "assets/images/img_search.png",
                                    width: 20,
                                    height: 25,
                                  ),
                                ),
                                Text(" "),
                                Container(
                                  width: 1,
                                  height: 20,
                                  color: Colors.grey[300],
                                ),
                                Text("  "),
                                Expanded(
                                  child: TextFormField(
                                    enableInteractiveSelection: false,
                                    decoration: InputDecoration(
                                        contentPadding:
                                            EdgeInsets.only(bottom: 12),
                                        border: InputBorder.none,
                                        hintText: "Tìm kiếm câu hỏi",
                                        hintStyle:
                                            TextStyle(color: Colors.grey[400])),
                                    // textAlignVertical: TextAlignVertical.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Expanded(child: Text("Danh mục")),
                            GestureDetector(
                              onTap: () {
                                displayBottomSheet(context);
                              },
                              child: Text(
                                "Xem tất cả danh mục",
                                style: TextStyle(color: Colors.blue),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.symmetric(horizontal: 1),
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: categorySeach.length + 1,
                          itemBuilder: (context, index) {
                            if (index == 0) {
                              return GestureDetector(
                                onTap: () {},
                                child: Row(
                                  children: [
                                    Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(500),
                                      ),
                                      child: Container(
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                3.6,
                                            height: MediaQuery.of(context)
                                                .size
                                                .height,
                                            color: Colors.blue[100],
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          500),
                                                  child: Container(
                                                    color: Colors.blue[300],
                                                    child: Image.asset(
                                                      "assets/images/more2.png",
                                                      height: 25,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  "Tất cả",
                                                  style: TextStyle(
                                                      color: Colors.black),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              );
                            }
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 10),
                              child: CategorySocial(
                                id: index - 1,
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Stack(
                    children: [
                      Container(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * 1,
                              height: MediaQuery.of(context).size.height * 0.02,
                              color: Colors.grey[100],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                WidgetListSocial(),
              ],
            ),
            Container(
              padding: EdgeInsets.only(bottom: 60),
              child: _buildContent(),
            )
          ],
        ),
      ),
    );
  }

  _buildContent() => WidgetCommentQuestions();
  _buildAppbar() => WidgetSocialAppbar();

  void displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
      backgroundColor: Colors.white,
      context: context,
      isScrollControlled: true,
      builder: (context) => Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height - 40,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    textNode.length <= 20
                        ? textNode
                        : textNode.substring(0, 20) + '...',
                    style: AppStyle.DEFAULT_SMALL.copyWith(
                        color: AppColor.BLACK, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.start,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Card(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      height: 30,
                      color: Colors.grey[100],
                      child: Center(
                        child: Row(
                          // mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Image.asset(
                                "assets/images/img_search.png",
                                width: 20,
                                height: 25,
                              ),
                            ),
                            Text(" "),
                            Container(
                              width: 1,
                              height: 20,
                              color: Colors.grey[300],
                            ),
                            Text("  "),
                            Expanded(
                              child: TextFormField(
                                enableInteractiveSelection: false,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(bottom: 12),
                                    border: InputBorder.none,
                                    hintText: "Tìm kiếm danh mục",
                                    hintStyle:
                                        TextStyle(color: Colors.grey[400])),
                                // textAlignVertical: TextAlignVertical.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: GridView.builder(
                            shrinkWrap: true,
                            padding:
                                EdgeInsets.only(top: 10, left: 5, right: 5),
                            itemBuilder: (context, index) {
                              return Container(
                                child: CategoryProfessional(
                                  id: index,
                                ),
                              );
                            },
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    crossAxisSpacing: 10,
                                    mainAxisSpacing: 5,
                                    childAspectRatio: 1.7),
                            // physics: NeverScrollableScrollPhysics(),
                            itemCount: itemCategory.length)))
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, top: 10),
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateBack();
              },
              child: Image.asset(
                "assets/images/img_close_round.png",
                height: 25,
                width: 25,
              ),
            ),
          )
        ],
      ),
    );
  }
}
