import 'package:base_code_project/model/enitity_offline/categoryseach.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategorySocial extends StatelessWidget {
  final int id;
  CategorySocial({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onTap: () => Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => DetailScreen(id: id)
      //   ),
      // ),
      child: Row(
        children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(500),
            ),
            child: Container(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Container(
                  width: MediaQuery.of(context).size.width / 3.2,
                  height: MediaQuery.of(context).size.height / 5,
                  color: Colors.grey[50],
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(500),
                        child: Container(
                          color: Colors.blue[300],
                          child: Image.asset(
                            categorySeach[id].imageTop,
                            height: 25,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        categorySeach[id].textTop,
                        style: TextStyle(color: Colors.grey),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),

    );
  }
}
