import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetSocialAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 20),
      child: WidgetAppbar(
        right: [
          Container(
            padding: EdgeInsets.only(right: 10),
            child: GestureDetector(
                onTap: () {
                  AppNavigator.navigateHistoryQuestion();
                },
                child: Text(
                  AppLocalizations.of(context).translate('social.history'),
                  style: TextStyle(color: Colors.blue, fontSize: 14),
                )),
          )
        ],
      ),
    );
  }
}
