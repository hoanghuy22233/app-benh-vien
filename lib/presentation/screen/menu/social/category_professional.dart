import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/model/enitity_offline/categoryseach.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryProfessional extends StatefulWidget {
  final int id;
  CategoryProfessional({Key key, this.id}) : super(key: key);

  @override
  _CategoryProfessionalState createState() => _CategoryProfessionalState();
}

class _CategoryProfessionalState extends State<CategoryProfessional> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: null,
      child: Card(
        elevation: 2,
        color: Colors.blue[100],
        child: Container(
          height: 60,
          width: MediaQuery.of(context).size.width / 2,
          child: Row(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                      color: Colors.blueGrey[100],
                      borderRadius: BorderRadius.circular(500),
                    ),
                    child: null,
                  ),
                  Image.asset(
                    '${categorySeach[widget.id].imageTop}',
                    width: 25,
                    height: 25,
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(left: 10, top: 10),
                child: Text(
                  categorySeach[widget.id].textTop.length <= 100
                      ? categorySeach[widget.id].textTop
                      : categorySeach[widget.id].textTop.substring(0, 100) +
                          '...',
                  style: AppStyle.DEFAULT_SMALL.copyWith(
                      color: AppColor.BLACK, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.start,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
