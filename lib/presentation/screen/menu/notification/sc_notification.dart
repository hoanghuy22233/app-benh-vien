import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/screen/news/sc_news.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NotificationScreen extends StatelessWidget {
  final GlobalKey<ScaffoldState> drawer;
  final Function moveTab;

  NotificationScreen({this.drawer, this.moveTab});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Container(
            child: Stack(
              children: [
                Container(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.03,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(
                    "Thông báo",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    height: 50,
                    color: Colors.lightBlue[200],
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/icons/nav_bell.png",
                          height: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 9,
                          child: Text(
                            "Nhận thông báo giúp eVietTiep chăm sóc sức khỏe bạn chu đáo",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                            maxLines: 2,
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                            "Cài đặt",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 1,
                  width: double.maxFinite,
                  color: Colors.grey[100],
                ),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () {
                    AppNavigator.navigateNewScreen();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Image.asset(
                        "assets/images/dad.png",
                        height: 25,
                      ),
                      // Expanded(
                      //   flex: 1,
                      //   child: Image.asset("assets/icons/nav_bell.png",height: 20,),
                      // ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        flex: 11,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Tư vấn & Hỏi đáp",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "Xem ngay phản hồi của Bác sĩ",
                              style: TextStyle(color: Colors.grey[500]),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 1,
                  width: double.maxFinite,
                  color: Colors.grey[100],
                ),
                Container(
                  height: 1,
                  width: double.maxFinite,
                  color: Colors.grey[100],
                ),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () => Navigator.push(context, MaterialPageRoute(builder:(context) => NewsScreen())),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      //
                      // Expanded(
                      //   flex: 1,
                      //   child: Image.asset("assets/icons/nav_bell.png",height: 20,),
                      // ),
                      Image.asset(
                        "assets/images/dad.png",
                        height: 25,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        flex: 12,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Tin tức sức khỏe",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "Bạn bỏ lỡ tin tức Sức khỏe và Y tế",
                              style: TextStyle(color: Colors.grey[500]),
                            )
                          ],
                        ),
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Container(
                            height: 20,
                            width: 40,
                            color: Colors.red[100],
                            child: Center(
                              child: Text(
                                "99+",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 12),
                                // textAlign: TextAlign.center,
                              ),
                            )),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 1,
                  width: double.maxFinite,
                  color: Colors.grey[100],
                ),
                Container(
                  height: 1,
                  width: double.maxFinite,
                  color: Colors.grey[100],
                ),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () {
                    AppNavigator.navigateNewScreen();
                  },
                  child:   Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      //
                      // Expanded(
                      //   flex: 1,
                      //   child: Image.asset("assets/icons/nav_bell.png",height: 20,),
                      // ),
                      Image.asset(
                        "assets/images/dad.png",
                        height: 25,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        flex: 12,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Khuyến mãi",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "Tin khuyến mãi và ưu đã mới nhất",
                              style: TextStyle(color: Colors.grey[500]),
                            )
                          ],
                        ),
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Container(
                            height: 20,
                            width: 30,
                            color: Colors.red[100],
                            child: Center(
                              child: Text(
                                "01",
                                style:
                                TextStyle(color: Colors.white, fontSize: 12),
                                // textAlign: TextAlign.center,
                              ),
                            )),
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 1,
                  width: double.maxFinite,
                  color: Colors.grey[100],
                ),
              ],
            ),
          ),
          Container(
            child: Stack(
              children: [
                Container(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 1,
                        height: MediaQuery.of(context).size.height * 0.03,
                        color: Colors.grey[50],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(
                    "Cập nhật lịch hẹn",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Image.asset(
                    "assets/icons/nav_bell.png",
                    height: 70,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                GestureDetector(
                  onTap: () {
                    AppNavigator.navigateServiceScreen();
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      height: 50,
                      color: Colors.lightBlue[500],
                      child: Center(
                        child: Text(
                          "Đặt lịch hẹn khám",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

        ],
      )),
    );
  }
}
