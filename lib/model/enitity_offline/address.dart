class Address {
  final String name;
  final String phoneNumber;
  final String homeAddress;
  final String city;
  final String district;
  final String ward;
  final bool isDefault;

  Address({this.name, this.phoneNumber, this.homeAddress, this.city, this.district, this.ward, this.isDefault});

}

List<Address> address = [
  Address (
    name: "huy",
    phoneNumber: "0357666888",
    homeAddress: "số 33",
    city: "Hải Phòng",
    district: "Đồ Sơn",
    ward: "Ngọc Sơn",
    isDefault: true,
  ),

  Address (
    name: "tùng",
    phoneNumber: "0357666999",
    homeAddress: "số 123",
    city: "Hải Phòng",
    district: "Đồ Sơn",
    ward: "Ngọc Sơn",
    isDefault: false,
  ),
];

