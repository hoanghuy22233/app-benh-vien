class ReasonChooseUs {
  final String image, title, detail;
  final int  free;
  ReasonChooseUs({
    this.title,
    this.image,
    this.detail,
    this.free,
});
}

List<ReasonChooseUs> reasonChooseUsItem =[
  ReasonChooseUs(
    image: 'assets/images/ic_detail_home.png',
    title: 'Lấy mẫu miễn phí tại nhà',
    detail: 'Điều dưỡng đến trực tiếp tại nhà bạn và lấy mẫu xét nghiệm',
    free: 1,
),
  ReasonChooseUs(
    image: 'assets/images/doctor.png',
    title: 'Bác sĩ tư vấn online miễn phí',
    detail: 'Bạn sẽ nhận được tư vấn từ các Bác sĩ uy tín và chuyên nghiệp của eVietTiep',
    free: 1
  ),
  ReasonChooseUs(
    image: 'assets/images/ic_detail_phone.png',
    title: 'Nhận kết quả trên điện thoại',
    detail: 'Kết quả xét nghiệm hiển thụ ngay trên thiết bị điện thoại của bạn',
    free: 0,
  ),
];