class User {
  final String avatar;
  final String name;
  final int relationship;
  final int gender;
  final String dob;
  final String phoneNumber;
  final String email;
  final bool isDefault;

  User({this.avatar, this.name, this.isDefault, this.relationship, this.gender, this.dob, this.phoneNumber, this.email});

}

List<User> user = [
  User (
    name: "Lê Thị Hằng",
    avatar: "assets/images/user.png",
    relationship: 0,
    gender: 1,
    dob: "20/10/1990",
    phoneNumber: "0357666888",
    email: "abc@gmail.com",
    isDefault: true,
  ),

  User (
    name: "Bùi Thị Nga",
    avatar: "assets/images/user.png",
    relationship: 2,
    gender: 0,
    dob: "11/12/1965",
    phoneNumber: "0357666777",
    email: "abc@gmail.com",
    isDefault: false,
  ),

  User (
    name: "Nguyễn Văn Long",
    avatar: "assets/images/user.png",
    relationship: 1,
    gender: 0,
    dob: "01/02/1966",
    phoneNumber: "0357666999",
    email: "abc@gmail.com",
    isDefault: false,
  ),
];

