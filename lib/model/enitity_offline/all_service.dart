import 'dart:developer';

class Service {
  final String image, title, describe, sex, age, xetnghiem, muckham, price, priceSale, discount;
  final int isDiscount;
  Service({
    this.image,
    this.title,
    this.describe,
    this.sex,
    this.age,
    this.xetnghiem,
    this.price,
    this.muckham,
    this.priceSale,
    this.discount,
    this.isDiscount,
  });
}

List<Service> serviceItem = [
  Service(
    image: "assets/images/img_doctorandpatient1.jpg",
    title: "Gói khám sức khoẻ cơ bản",
    describe: "Kiếm tra sức khoẻ cơ bản qua các xét nghiệm thường quy, điện tim và X-quang phổi, dành cho người trưởng thành ",
    sex: "Nam & Nữ",
    age: "Mọi lứa tuổi",
    xetnghiem: "13 xét nghiệm",
    muckham: "4 hạng mục khám",
    price: "1.490.000 đ",
    isDiscount: 0,
  ),
  Service(
    image: "assets/images/img_doctorandpatient5.jpg",
    title: "Gói xét nghiệm tổng quát",
    describe: "Gói xét nghiệm tổng quát bao gồn các xét nghiệm cơ bản như máu, đường huyết, mỡ máu, chức năng khác trong cơ thể",
    sex: "Nam & Nữ",
    age: "Mọi lứa tuổi",
    xetnghiem: "13 xét nghiệm",
    price: "850.000 đ",
    isDiscount: 0,
  ),
  Service(
    image: "assets/images/img_doctorandpatient5.jpg",
    title: "Gói tầm soát ung thư cổ tử cung",
    describe: "Tầm soát Ung thư cổ tử cung bằng xết nghiệm sự hiện diện của virus HPV, nguyên nhân gây ra hơn 99% ung thư,",
    sex: "Nữ",
    age: "Mọi lứa tuổi",
    xetnghiem: "1 xét nghiệm",
    muckham: "1 hạng mục khám",
    price: "850.000 đ",
    isDiscount: 0,
  ),
  Service(
      image: "assets/images/img_doctorandpatient2.jpg",
      title: "Gói xét nghiệm gan",
      describe: "Xét nghiệm đánh giá chức năng gan, khả năng nguy cơ bị viêm gan B, xơ gan, ung thư gan.",
      sex: "Nam & Nữ",
      age: "Mọi lứa tuổi",
      xetnghiem: "4 xét nghiệm",
      price: "600.000 đ",
      discount: "18%",
      priceSale: "490.000 đ",
    isDiscount: 1,
  ),
  Service(
      image: "assets/images/img_doctorandpatient3.jpg",
      title: "Gói xét nghiệm tổng quát",
      describe: "Gói xét nghiệm tổng quát bao gồm các xét nghiệm cơ bản như máu, đường huyết, mỡ máu, chức năng cơ bản fjfjfjfjfjfjfjfj",
      sex: "Nam & Nữ",
      age: "Mọi lứa tuổi",
      xetnghiem: "13 xét nghiệm",
      price: "425000",
      discount: "50%",
      isDiscount: 1,
      priceSale: "850.000 đ"),

  Service(
      image: "assets/images/img_doctorandpatient4.jpg",
      title: "Xét nghiệm tầm soát ung thư nữ",
      describe: "Hiện nay, có thể nói ung thư là căn bệnh nguy hiểm nhất, khó chữa nhất đã cướp đi tính mạng của biết bao nhiêu người trong cuộc chiến chống lại ung thư.",
      sex: "Nữ",
      age: "Mọi lứa tuổi",
      xetnghiem: "6 xét nghiệm",
      isDiscount: 1,
      price: "600.000 đ",
      discount: "18%",
      priceSale: "490.000 đ"),
];
