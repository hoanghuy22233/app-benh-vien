class ItemDoctor {
  final String name;
  final double star;
  final String avatar;
  final String type;

  ItemDoctor({
    this.name, this.star, this.avatar, this.type,
  });
}

List<ItemDoctor> doctorList = [
  ItemDoctor (
    name: "BS. Lại Xuân Lợi",
    star: 5,
    avatar: "assets/images/avatar_doctor.png",
    type: "Ung bướu",
  ),
  ItemDoctor (
      name: "BS. Trần Quang Hưng",
      star: 4.8,
      avatar: "assets/images/avatar_doctor.png",
      type: "Nội khoa",
  ),
  ItemDoctor (
    name: "BS. Nguyễn Thị Lan",
    star: 5,
    avatar: "assets/images/avatar_doctor.png",
    type: "Y học cổ truyền",
  ),

];

