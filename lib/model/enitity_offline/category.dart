class CategoryItem {
  final String image, name, colorImage, background;
  CategoryItem({this.image, this.colorImage, this.name, this.background});
}

List<CategoryItem> itemCategory = [
  CategoryItem(
      name: "Khám tổng quan",
      image: "assets/images/ic_heart.png",
      colorImage: "FFE57373",
      background: "assets/images/doctor_new.jpg"),
  CategoryItem(
      name: "Xét nghiệm tại nhà",
      image: "assets/images/houses.png",
      colorImage: "FF00E676",
      background: "assets/images/doctor_new.jpg"),
  CategoryItem(
      name: "Chat với bác sĩ",
      image: "assets/images/ic_chat.png",
      colorImage: "FFFFD180",
      background: "assets/images/doctor_new.jpg"),
  CategoryItem(
      name: "Xét nghiệm di truyền",
      image: "assets/images/ic_dna.png",
      colorImage: "FF00E5FF",
      background: "assets/images/doctor_new.jpg"),
  CategoryItem(
      name: "Thông tin sức khoẻ",
      image: "assets/images/ic_information.png",
      colorImage: "FFEA80FC",
      background: "assets/images/doctor_new.jpg"),
  CategoryItem(
      name: "Hỏi bác sĩ",
      image: "assets/images/ic_ask.png",
      colorImage: "FF80D8FF",
      background: "assets/images/doctor_new.jpg"),
  CategoryItem(
      name: "Sức khoẻ & Sắc đẹp",
      image: "assets/images/ic_lipstick.png",
      colorImage: "FFD500F9",
      background: "assets/images/doctor_new.jpg"),
];
