class CategoryNews {
  final int id;
  final String name;
  CategoryNews({this.id, this.name});
}

List<CategoryNews> categoryNews = [
  CategoryNews(
    id: 0,
    name: "Tất cả",
  ),
  CategoryNews(
    id: 1,
    name: "Tin tức",
  ),
  CategoryNews(
    id: 2,
    name: "Ưu đãi",
  ),
  CategoryNews(
    id: 3,
    name: "Bí quyết sống",
  ),
];
