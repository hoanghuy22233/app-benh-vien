class News {
  final String title, content, time;
  News({
    this.title,
    this.content,
    this.time
  });
}

List<News> newsItem = [
  News (
    title: "dấu hiệu cho thấy cơ thể bạn đang kêu cứu",
    content: "Lúc nào cũng thèm nhai đá hoặc ăn muối có thể là dấu hiệu của một số vấn đề nghiêm trọng về sức khoẻ hoặc thiếu khoáng chất và vitamin, và các chuyên ra lưu ý phải",
    time: "2 giờ trước"
  ),
  News (
    title: "[CLIP] Là phổi đen kịt sau khi hút 20 điếu thuốc lá",
    content: "Sau khi hít vào lượng khói của một gói thuốc lá, màu lá phổi gần như biến đổi hoàn toàn với lớp cao đen bao phủ khí quản. Xem clip >>",
    time: "11 giờ trước"

  ),
  News (
    title: "Trung Quốc dùng thảo dược điều trị Covid-19",
    content: "Tuy nhiên, ý tưởng đã vấp phải sự chỉ trích của nhiều chuyên gia người phương Tây. Hồi đầu tháng 5, mỗt bác sĩ người Canada đã cảnh báo thận trọng khi sử dụng",
    time: "một ngày trước",
  ),
  News (
    title: "10 thói quen khiến bạn mau già",
    content: "Nhũng thói quan hằng ngày gây nên tình trạng lão hoá sớm mà có thể bạn đã và đang mắc phải. Bỏ ngay 10 thói quen không ngờ này nếu bạn không muốn mau già.",
    time: "một ngày trước",
  ),
  News(
    title: "Bí quyết bổ sung nguồn vitamin D tự nhiên cho cơ thể",
    content: "Vitamin D là một chất dinh dưỡng đóng vai trò thiết yếu trong việc duy trì cho bạn một cơ thể khoẻ mạnh. Xem chi tiết >>",
    time: "2 ngày trước"
  )

];
