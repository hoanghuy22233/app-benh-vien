import 'package:base_code_project/presentation/router.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppNavigator {
  AppNavigator._();

  static navigateBack() async {
    Get.back();
  }

  static navigatePopUtil({String name}) async {
    Navigator.popUntil(Get.context, ModalRoute.withName(name));
  }

  static navigateSplash() async {
    var result = await Get.toNamed(BaseRouter.SPLASH);
    return result;
  }

  static navigateLogin() async {
    var result = await Get.offAllNamed(BaseRouter.LOGIN);
    return result;
  }

  /////
  static navigateEnterPhoneNumber() async {
    var result = await Get.toNamed(BaseRouter.ENTERPHONENUMBER);
    return result;
  }

  static navigateNavigation() async {
    var result = await Get.offAllNamed(BaseRouter.NAVIGATION);
    return result;
  }

  static navigateHistoryQuestion() async {
    var result = await Get.toNamed(BaseRouter.HISTORY_QUESTION);
    return result;
  }

  static navigateHistoryAdvisory() async {
    var result = await Get.toNamed(BaseRouter.HISTORY_ADVISORY);
    return result;
  }

  static navigateFamilyProfile() async {
    var result = await Get.toNamed(BaseRouter.FAMILY_PROFILE);
    return result;
  }

  static navigateAddPerson() async {
    var result = await Get.toNamed(BaseRouter.ADD_PERSON);
    return result;
  }

  static navigateRegisterPage() async {
    var result = await Get.toNamed(BaseRouter.REGISTER_PAGE);
    return result;
  }

  static navigateForgotPass() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASS);
    return result;
  }

  static navigateForgotPassVerifi() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASS_VERIFICAL);
    return result;
  }

  static navigateForgotPassReset() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASS_RESET);
    return result;
  }

  static navigateRegisterVerify({String username, String phone}) async {
    var result = await Get.toNamed(BaseRouter.REGISTER_VERIFY,
        arguments: {'username': username, 'phone': phone});
    return result;
  }

  static navigateChatWithDoctor() async {
    var result = await Get.toNamed(BaseRouter.CHAT_WITH_DOCTOR);
    return result;
  }

  static navigateTestResult() async {
    var result = await Get.toNamed(BaseRouter.TEST_RESULT);
    return result;
  }

  static navigateListDoctor() async {
    var result = await Get.toNamed(BaseRouter.LIST_DOCTOR);
    return result;
  }

  static navigateListNutrition() async {
    var result = await Get.toNamed(BaseRouter.LIST_NUTRITION);
    return result;
  }

  static navigateListDetailComment() async {
    var result = await Get.toNamed(BaseRouter.LIST_COMMENT_DETAIL);
    return result;
  }

  static navigateNewPost() async {
    var result = await Get.toNamed(BaseRouter.NEWS_POST);
    return result;
  }

  static navigateNewScreen() async {
    var result = await Get.toNamed(BaseRouter.NEWS_SCREEN);
    return result;
  }

  static navigateAddressScreen() async {
    var result = await Get.toNamed(BaseRouter.ADDRESS_SCREEN);
    return result;
  }

  static navigateAddNewAddressScreen() async {
    var result = await Get.toNamed(BaseRouter.ADD_ADDRESS_SCREEN);
    return result;
  }

  static navigateEditAddressScreen() async {
    var result = await Get.toNamed(BaseRouter.EDIT_ADDRESS_SCREEN);
    return result;
  }

  static navigateNewSeachQuestionScreen() async {
    var result = await Get.toNamed(BaseRouter.SEACH_QUESTION_SCREEN);
    return result;
  }
  static navigateNewNotificationAccountScreen() async {
    var result = await Get.toNamed(BaseRouter.NOTIFICATION_ACCOUNT_SCREEN);
    return result;
  }
  static navigateNewCalendarReminderScreen() async {
    var result = await Get.toNamed(BaseRouter.CALENDAR_REMINDER_SCREEN);
    return result;
  }
  static navigateServiceScreen() async {
    var result = await Get.toNamed(BaseRouter.SERVICE_SCREEN);
    return result;
  }
  // static navigateDetailServiceScreen({int id}) async {
  //   var result = await Get.toNamed(BaseRouter.DETAIL_SERVICE, arguments: {
  //     'id': id
  //   });
  //   return result;
  // }

}
